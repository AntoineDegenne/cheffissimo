package fr.afpa.cheffissimo.model;

import java.util.List;

public class RecipeAudit {
    private int id;
    private int recipeId;
    private Recipe recipeOld;
    private String date;
    private List<Component> components;

    public RecipeAudit() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(int recipeId) {
        this.recipeId = recipeId;
    }

    public Recipe getRecipeOld() {
        return recipeOld;
    }

    public void setRecipeOld(Recipe recipeOld) {
        this.recipeOld = recipeOld;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    @Override
    public String toString() {
        return date;
    }
}
