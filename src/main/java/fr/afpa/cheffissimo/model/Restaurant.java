package fr.afpa.cheffissimo.model;

public class Restaurant {
    private int id;
    private String name;
    private String address;
    private String city;
    private String phone;
    private String mail;

    public Restaurant() {
    }

    public Restaurant(String name, String address, String city, String phone, String mail) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.phone = phone;
        this.mail = mail;
    }

    public Restaurant(int id, String name, String address, String city, String phone, String mail) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.city = city;
        this.phone = phone;
        this.mail = mail;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @Override
    public boolean equals(Object arg0) {
        Restaurant restaurant = (Restaurant) arg0;
        return (this.name.equals(restaurant.name));
    }

    @Override
    public String toString() {
        return "Restaurant [id=" + id + ", name=" + name + ", address=" + address + ", city=" + city + ", phone="
                + phone + ", mail=" + mail + "]";
    }
}
