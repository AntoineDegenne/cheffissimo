package fr.afpa.cheffissimo.model;

import fr.afpa.cheffissimo.util.FloatUtil;

public class Component {
    private int id;
    private String name;
    private float quantity;
    private String unit;
    private Recipe recipe;

    public Component() {
    }

    public Component(String name, float quantity, String unit) {
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
    }

    public Component(String name, float quantity, String unit, Recipe recipe) {
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
        this.recipe = recipe;
    }

    public Component(int id, String name, float quantity, String unit, Recipe recipe) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
        this.unit = unit;
        this.recipe = recipe;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getQuantity() {
        return quantity;
    }

    public void setQuantity(float quantity) {
        this.quantity = quantity;
    }

    public String getUnit() {
        if (unit == null)
            unit = "";
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    @Override
    public String toString() {

        if (unit == null)
            unit = "";
        String quantityString = (quantity == 0) ? "" : ": " + FloatUtil.roundFloat(quantity) + " " + unit;

        return name + quantityString;
    }
}
