package fr.afpa.cheffissimo.model.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.cheffissimo.model.Chef;
import fr.afpa.cheffissimo.model.Comment;
import fr.afpa.cheffissimo.model.Recipe;

public class CommentDAO implements DAO<Comment> {

    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
    ChefDAO chefDAO = new ChefDAO();

    @Override
    public Comment getById(int id) {
        // La table comment n'a pas de colonne id
        throw new UnsupportedOperationException("The column (id) doesn't exist for the table \"comment\"");
    }

    public Comment getByChefAndRecipe(Chef chef, Recipe recipe) {
        Comment comment = new Comment();

        try {
            PreparedStatement stm = connection
                    .prepareStatement("SELECT * FROM comment WHERE chef_id = ? AND recipe_id = ?");
            stm.setInt(1, chef.getId());
            stm.setInt(2, recipe.getId());
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                comment.setContent(rs.getString("content"));
                comment.setRating(rs.getInt("rating"));
                comment.setDate(rs.getTimestamp("update_time").toLocalDateTime().format(formatter));
                comment.setChef(chef);
                comment.setRecipe(recipe);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return comment;
    }

    @Override
    public List<Comment> getAll() {
        RecipeDAO recipeDAO = new RecipeDAO();
        List<Comment> comments = new ArrayList<>();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM comment");

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Comment comment = new Comment();
                comment.setContent(rs.getString("content"));
                comment.setRating(rs.getInt("rating"));
                comment.setDate(rs.getTimestamp("update_time").toLocalDateTime().format(formatter));
                comment.setRecipe(recipeDAO.getById(rs.getInt("recipe_id")));
                comment.setChef(chefDAO.getById(rs.getInt("chef_id")));
                comments.add(comment);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return comments;
    }

    public List<Comment> getAllByRecipe(Recipe recipe) {
        List<Comment> comments = new ArrayList<>();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM comment WHERE recipe_id = ?");
            stm.setInt(1, recipe.getId());
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Comment comment = new Comment();
                comment.setContent(rs.getString("content"));
                comment.setRating(rs.getInt("rating"));
                comment.setDate(rs.getTimestamp("update_time").toLocalDateTime().format(formatter));
                comment.setRecipe(recipe);
                comment.setChef(chefDAO.getById(rs.getInt("chef_id")));
                comments.add(comment);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return comments;
    }

    @Override
    public boolean save(Comment comment) {
        int check = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "INSERT INTO comment(content, rating, recipe_id, chef_id) VALUES (?, ?, ?, ?)");
            stm.setString(1, comment.getContent());
            stm.setInt(2, comment.getRating());
            stm.setInt(3, comment.getRecipe().getId());
            stm.setInt(4, comment.getChef().getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return (check != 0);
    }

    @Override
    public boolean update(Comment comment) {
        int check = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "UPDATE comment SET content = ?, rating = ? WHERE recipe_id = ? AND chef_id = ?");
            stm.setString(1, comment.getContent());
            stm.setInt(2, comment.getRating());
            stm.setInt(3, comment.getRecipe().getId());
            stm.setInt(4, comment.getChef().getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return (check != 0);
    }

    @Override
    public boolean delete(Comment comment) {
        int check = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "DELETE FROM comment WHERE recipe_id = ? AND chef_id = ?");
            stm.setInt(1, comment.getRecipe().getId());
            stm.setInt(2, comment.getChef().getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return (check != 0);
    }

}
