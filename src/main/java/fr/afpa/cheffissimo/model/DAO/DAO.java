package fr.afpa.cheffissimo.model.DAO;

import java.sql.Connection;
import java.util.List;

import fr.afpa.cheffissimo.model.ConnectionPostgreSQL;

public interface DAO<T> {
    public Connection connection = ConnectionPostgreSQL.getInstance();

    /**
     * Permet de récupérer un objet via son id
     * 
     * @param id
     * @return
     */
    public abstract T getById(int id);

    /**
     * Permet de récupérer tous les objets dans une liste
     * 
     * @return
     */
    public abstract List<T> getAll();

    /**
     * Permet de créer une entrée dans la base de données
     * Renvoie true si réussi ou false si échoué
     * 
     * @param t
     * @return
     */
    public abstract boolean save(T t);

    /**
     * Permet de mettre à jour une entrée dans la base de données
     * Renvoie true si réussi et false si échoué
     * 
     * @param t
     * @return
     */
    public abstract boolean update(T t);

    /**
     * Permet de supprimer une entrée dans la base de donnée
     * Renvoie true si réussi et false si échoué
     * 
     * @param t
     * @return
     */
    public abstract boolean delete(T t);

}
