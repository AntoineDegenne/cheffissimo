package fr.afpa.cheffissimo.model.DAO;

import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.cheffissimo.model.Restaurant;

public class RestaurantDAO implements DAO<Restaurant> {

    @Override
    public Restaurant getById(int id) {

        Restaurant restaurant = new Restaurant();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM restaurant WHERE restaurant_id = ?");
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                restaurant.setId(rs.getInt("restaurant_id"));
                restaurant.setName(rs.getString("name"));
                restaurant.setAddress(rs.getString("address"));
                restaurant.setCity(rs.getString("city"));
                restaurant.setPhone(rs.getString("phone"));
                restaurant.setMail(rs.getString("mail"));
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return restaurant;

    }

    @Override
    public List<Restaurant> getAll() {
        List<Restaurant> restaurants = new ArrayList<>();
        try {
            Statement stm = connection.createStatement();
            ResultSet rs = stm.executeQuery("SELECT * from restaurant");
            while (rs.next()) {
                Restaurant restaurant = new Restaurant();
                restaurant.setId(rs.getInt("restaurant_id"));
                restaurant.setName(rs.getString("name"));
                restaurant.setAddress(rs.getString("address"));
                restaurant.setCity(rs.getString("city"));
                restaurant.setPhone(rs.getString("phone"));
                restaurant.setMail(rs.getString("mail"));
                restaurants.add(restaurant);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return restaurants;
    }

    @Override
    public boolean save(Restaurant restaurant) {

        int id = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "INSERT INTO restaurant(name, address, city, phone, mail) VALUES (?,?,?,?,?) RETURNING restaurant_id");
            stm.setString(1, restaurant.getName());
            stm.setString(2, restaurant.getAddress());
            stm.setString(3, restaurant.getCity());
            stm.setString(4, restaurant.getPhone());
            stm.setString(5, restaurant.getMail());

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        restaurant.setId(id);
        return (id != 0);
    }

    @Override
    public boolean update(Restaurant restaurant) {
        int check = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "UPDATE restaurant SET name = ?, address = ?, city = ?, phone = ?, mail = ? WHERE restaurant_id = ?");
            stm.setString(1, restaurant.getName());
            stm.setString(2, restaurant.getAddress());
            stm.setString(3, restaurant.getCity());
            stm.setString(4, restaurant.getPhone());
            stm.setString(5, restaurant.getMail());
            stm.setInt(6, restaurant.getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (check != 0);
    }

    @Override
    public boolean delete(Restaurant restaurant) {

        int check = 0;

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "DELETE FROM restaurant WHERE restaurant_id = ?");
            stm.setInt(1, restaurant.getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (check != 0);
    }
}
