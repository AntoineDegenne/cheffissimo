package fr.afpa.cheffissimo.model.DAO;

import java.sql.Array;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.cheffissimo.model.Chef;
import fr.afpa.cheffissimo.model.Component;
import fr.afpa.cheffissimo.model.Recipe;
import fr.afpa.cheffissimo.model.RecipeAudit;

public class RecipeDAO implements DAO<Recipe> {

    final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm");
    private ChefDAO chefDAO = new ChefDAO();
    private CommentDAO commentDAO = new CommentDAO();

    @Override
    public Recipe getById(int id) {

        Recipe recipe = new Recipe();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM recipe WHERE recipe_id = ?");
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                recipe.setId(rs.getInt("recipe_id"));
                recipe.setName(rs.getString("name"));
                recipe.setTotalTime(rs.getTime("total_time"));
                recipe.setServingsNb(rs.getInt("servings_nb"));
                recipe.setType(rs.getString("type"));
                recipe.setVegeBool(rs.getBoolean("vege_bool"));
                Array steps = rs.getArray("steps");
                recipe.setSteps((String[]) steps.getArray());
                recipe.setChef(chefDAO.getById(rs.getInt("chef_id")));
                recipe.calculateRating(commentDAO.getAllByRecipe(recipe));
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return recipe;
    }

    @Override
    public List<Recipe> getAll() {
        List<Recipe> recipes = new ArrayList<>();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM recipe");

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Recipe recipe = new Recipe();
                recipe.setId(rs.getInt("recipe_id"));
                recipe.setName(rs.getString("name"));
                recipe.setTotalTime(rs.getTime("total_time"));
                recipe.setServingsNb(rs.getInt("servings_nb"));
                recipe.setType(rs.getString("type"));
                recipe.setVegeBool(rs.getBoolean("vege_bool"));
                Array steps = rs.getArray("steps");
                recipe.setSteps((String[]) steps.getArray());
                recipe.setChef(chefDAO.getById(rs.getInt("chef_id")));
                recipe.calculateRating(commentDAO.getAllByRecipe(recipe));

                recipes.add(recipe);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return recipes;
    }

    public List<Recipe> getAllByChef(Chef chef) {
        List<Recipe> recipes = new ArrayList<>();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM recipe WHERE chef_id = ?");
            stm.setInt(1, chef.getId());
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Recipe recipe = new Recipe();
                recipe.setId(rs.getInt("recipe_id"));
                recipe.setName(rs.getString("name"));
                recipe.setTotalTime(rs.getTime("total_time"));
                recipe.setServingsNb(rs.getInt("servings_nb"));
                recipe.setType(rs.getString("type"));
                recipe.setVegeBool(rs.getBoolean("vege_bool"));
                Array steps = rs.getArray("steps");
                recipe.setSteps((String[]) steps.getArray());
                recipe.setChef(chef);
                recipe.calculateRating(commentDAO.getAllByRecipe(recipe));
                recipes.add(recipe);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return recipes;
    }

    public List<Recipe> getAllByType(String string) {
        List<Recipe> recipes = new ArrayList<>();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM recipe WHERE type LIKE ?");
            stm.setString(1, string.toUpperCase());
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Recipe recipe = new Recipe();
                recipe.setId(rs.getInt("recipe_id"));
                recipe.setName(rs.getString("name"));
                recipe.setTotalTime(rs.getTime("total_time"));
                recipe.setServingsNb(rs.getInt("servings_nb"));
                recipe.setType(rs.getString("type"));
                recipe.setVegeBool(rs.getBoolean("vege_bool"));
                Array steps = rs.getArray("steps");
                recipe.setSteps((String[]) steps.getArray());
                recipe.setChef(chefDAO.getById(rs.getInt("chef_id")));
                recipe.calculateRating(commentDAO.getAllByRecipe(recipe));
                recipes.add(recipe);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return recipes;
    }

    public List<Recipe> getAllByChefByType(Chef chef, String string) {
        List<Recipe> recipes = new ArrayList<>();

        try {
            PreparedStatement stm = connection
                    .prepareStatement("SELECT * FROM recipe WHERE chef_id = ? AND type LIKE ?");
            stm.setInt(1, chef.getId());
            stm.setString(2, string.toUpperCase());
            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Recipe recipe = new Recipe();
                recipe.setId(rs.getInt("recipe_id"));
                recipe.setName(rs.getString("name"));
                recipe.setTotalTime(rs.getTime("total_time"));
                recipe.setServingsNb(rs.getInt("servings_nb"));
                recipe.setType(rs.getString("type"));
                recipe.setVegeBool(rs.getBoolean("vege_bool"));
                Array steps = rs.getArray("steps");
                recipe.setSteps((String[]) steps.getArray());
                recipe.setChef(chef);
                recipe.calculateRating(commentDAO.getAllByRecipe(recipe));
                recipes.add(recipe);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return recipes;
    }

    public List<RecipeAudit> getHistory(Recipe recipe) {
        List<RecipeAudit> oldRecipes = new ArrayList<>();

        try {
            PreparedStatement stm = connection
                    .prepareStatement("SELECT * FROM recipe_audit WHERE recipe_id = ? AND event_type LIKE 'UPDATE' ORDER BY timestamp DESC");
            stm.setInt(1, recipe.getId());

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                RecipeAudit recipeAudit = new RecipeAudit();
                List<Component> components = new ArrayList<>();
                Recipe oldRecipe = new Recipe();
                recipeAudit.setId(rs.getInt("recipe_audit_id"));
                recipeAudit.setRecipeId(rs.getInt("recipe_id"));
                recipeAudit.setDate(rs.getTimestamp("timestamp").toLocalDateTime().format(formatter));
                oldRecipe.setName(rs.getString("name"));
                oldRecipe.setTotalTime(rs.getTime("total_time"));
                oldRecipe.setServingsNb(rs.getInt("servings_nb"));
                oldRecipe.setType(rs.getString("type"));
                oldRecipe.setVegeBool(rs.getBoolean("vege_bool"));
                Array steps = rs.getArray("steps");
                oldRecipe.setSteps((String[]) steps.getArray());
                oldRecipe.setChef(chefDAO.getById(rs.getInt("chef_id")));

                String[] componentsNames = (String[]) rs.getArray("components_names").getArray();
                Float[] componentsQuantities = (Float[]) rs.getArray("components_quantities").getArray();
                String[] componentsUnits = (String[]) rs.getArray("components_units").getArray();
                for(int i = 0; i < componentsNames.length; i++){
                    Component component = new Component(componentsNames[i], componentsQuantities[i], componentsUnits[i]);
                    components.add(component);
                }
                recipeAudit.setComponents(components);

                recipeAudit.setRecipeOld(oldRecipe);
                oldRecipes.add(recipeAudit);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oldRecipes;
    }

    @Override
    public boolean save(Recipe recipe) {
        int id = 0;

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "INSERT INTO recipe(name, total_time, servings_nb, type, vege_bool, steps, chef_id) VALUES (?, ?, ?, ?, ?, ?, ?) RETURNING recipe_id");
            stm.setString(1, recipe.getName());
            stm.setTime(2, recipe.getTotalTime());
            stm.setInt(3, recipe.getServingsNb());
            stm.setString(4, recipe.getType());
            stm.setBoolean(5, recipe.isVegeBool());
            stm.setArray(6, connection.createArrayOf("text", recipe.getSteps()));
            stm.setInt(7, recipe.getChef().getId());

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        recipe.setId(id);
        return (id != 0);
    }

    @Override
    public boolean update(Recipe recipe) {
        int check = 0;

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "UPDATE recipe SET name = ?, total_time = ?, servings_nb = ?, type = ?, vege_bool = ?, steps = ?, chef_id = ? WHERE recipe_id = ?");
            stm.setString(1, recipe.getName());
            stm.setTime(2, recipe.getTotalTime());
            stm.setInt(3, recipe.getServingsNb());
            stm.setString(4, recipe.getType());
            stm.setBoolean(5, recipe.isVegeBool());
            stm.setArray(6, connection.createArrayOf("text", recipe.getSteps()));
            stm.setInt(7, recipe.getChef().getId());
            stm.setInt(8, recipe.getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return (check != 0);
    }

    @Override
    public boolean delete(Recipe recipe) {
        int check = 0;

        try {
            PreparedStatement stm = connection.prepareStatement(
                    "DELETE FROM recipe WHERE recipe_id = ?");
            stm.setInt(1, recipe.getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return (check != 0);
    }

}
