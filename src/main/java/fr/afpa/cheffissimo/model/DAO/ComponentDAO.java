package fr.afpa.cheffissimo.model.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.cheffissimo.model.Component;
import fr.afpa.cheffissimo.model.Recipe;

public class ComponentDAO implements DAO<Component> {

    private RecipeDAO recipeDAO = new RecipeDAO();

    @Override
    public Component getById(int id) {

        Component component = new Component();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM component where component_id = ?");
            stm.setInt(1, id);

            ResultSet rs = stm.executeQuery();

            if (rs.next()) {
                component.setId(rs.getInt("component_id"));
                component.setName(rs.getString("name"));
                component.setQuantity(rs.getFloat("quantity"));
                component.setUnit(rs.getString("unit"));
                component.setRecipe(recipeDAO.getById(rs.getInt("recipe_id")));
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return component;
    }

    @Override
    public List<Component> getAll() {
        List<Component> components = new ArrayList<>();
        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM component");

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Component component = new Component();
                component.setId(rs.getInt("component_id"));
                component.setName(rs.getString("name"));
                component.setQuantity(rs.getFloat("quantity"));
                component.setUnit(rs.getString("unit"));
                component.setRecipe(recipeDAO.getById(rs.getInt("recipe_id")));
                components.add(component);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return components;
    }

    public List<Component> getAllByRecipe(Recipe recipe) {
        List<Component> components = new ArrayList<>();
        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM component WHERE recipe_id = ?");
            stm.setInt(1, recipe.getId());

            ResultSet rs = stm.executeQuery();

            while (rs.next()) {
                Component component = new Component();
                component.setId(rs.getInt("component_id"));
                component.setName(rs.getString("name"));
                component.setQuantity(rs.getFloat("quantity"));
                component.setUnit(rs.getString("unit"));
                component.setRecipe(recipeDAO.getById(rs.getInt("recipe_id")));
                components.add(component);
            }
        } catch (SQLException e) {

            e.printStackTrace();
        }
        return components;
    }

    @Override
    public boolean save(Component component) {
        int id = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "INSERT INTO component(name, quantity, unit, recipe_id) VALUES (?,?,?,?) RETURNING component_id");
            stm.setString(1, component.getName());
            stm.setFloat(2, component.getQuantity());
            stm.setString(3, component.getUnit());
            stm.setInt(4, component.getRecipe().getId());

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        component.setId(id);
        return (id != 0);
    }

    @Override
    public boolean update(Component component) {
        int check = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "UPDATE component SET name = ?, quantity = ?, unit = ?, recipe_id = ? WHERE component_id = ?");
            stm.setString(1, component.getName());
            stm.setFloat(2, component.getQuantity());
            stm.setString(3, component.getUnit());
            stm.setInt(4, component.getRecipe().getId());
            stm.setInt(5, component.getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return (check != 0);
    }

    @Override
    public boolean delete(Component component) {
        int check = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "DELETE FROM component WHERE component_id = ?");
            stm.setInt(1, component.getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return (check != 0);
    }
}
