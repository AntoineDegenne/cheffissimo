package fr.afpa.cheffissimo.model.DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.cheffissimo.model.Chef;
import fr.afpa.cheffissimo.model.ConnectedUser;
import fr.afpa.cheffissimo.model.Restaurant;

public class ChefDAO implements DAO<Chef> {

    private ConnectedUser connectedUser = ConnectedUser.getInstance();
    private RestaurantDAO restaurantDAO = new RestaurantDAO();

    public int checkCredentials(String mail, String password) {
        int id = 0;
        try {
            PreparedStatement stm = connection.prepareStatement("select * from user_connection(?, ?)");
            stm.setString(1, mail);
            stm.setString(2, password);
            ResultSet rs = stm.executeQuery();
            SQLWarning sqlWarning = stm.getWarnings();
            if (rs.next()) {
                id = rs.getInt(1);
            }

            if (sqlWarning != null) {
                connectedUser.setErrorMessage(sqlWarning.getMessage());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return id;
    }

    @Override
    public Chef getById(int id) {
        Chef chef = new Chef();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM chef WHERE chef_id = ?");
            stm.setInt(1, id);
            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                chef.setId(rs.getInt("chef_id"));
                chef.setLastName(rs.getString("last_name"));
                chef.setFirstName(rs.getString("first_name"));
                chef.setMail(rs.getString("mail"));
                chef.setPassword(rs.getString("password"));
                chef.setRestaurant(restaurantDAO.getById(rs.getInt("restaurant_id")));
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return chef;
    }

    @Override
    public List<Chef> getAll() {
        List<Chef> chefs = new ArrayList<>();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM chef");

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Chef chef = new Chef();
                chef.setId(rs.getInt("chef_id"));
                chef.setLastName(rs.getString("last_name"));
                chef.setFirstName(rs.getString("first_name"));
                chef.setMail(rs.getString("mail"));
                chef.setPassword(rs.getString("password"));
                chef.setRestaurant(restaurantDAO.getById(rs.getInt("restaurant_id")));
                chefs.add(chef);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return chefs;
    }

    public List<Chef> getAllByRestaurant(Restaurant restaurant) {
        List<Chef> chefs = new ArrayList<>();

        try {
            PreparedStatement stm = connection.prepareStatement("SELECT * FROM chef WHERE restaurant_id = ?");
            stm.setInt(1, restaurant.getId());

            ResultSet rs = stm.executeQuery();
            while (rs.next()) {
                Chef chef = new Chef();
                chef.setId(rs.getInt("chef_id"));
                chef.setLastName(rs.getString("last_name"));
                chef.setFirstName(rs.getString("first_name"));
                chef.setMail(rs.getString("mail"));
                chef.setPassword(rs.getString("password"));
                chef.setRestaurant(restaurant);
                chefs.add(chef);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return chefs;
    }

    @Override
    public boolean save(Chef chef) {
        int id = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "INSERT INTO chef(last_name, first_name, mail, password, restaurant_id) VALUES (?,?,?,?,?) RETURNING chef_id");
            stm.setString(1, chef.getLastName());
            stm.setString(2, chef.getFirstName());
            stm.setString(3, chef.getMail());
            stm.setString(4, chef.getPassword());
            stm.setInt(5, chef.getRestaurant().getId());

            ResultSet rs = stm.executeQuery();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            rs.close();
            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        chef.setId(id);
        return (id != 0);
    }

    @Override
    public boolean update(Chef chef) {
        int check = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "UPDATE chef SET last_name = ?, first_name = ?, mail = ?, password = ?, restaurant_id = ? WHERE chef_id = ?");
            stm.setString(1, chef.getLastName());
            stm.setString(2, chef.getFirstName());
            stm.setString(3, chef.getMail());
            stm.setString(4, chef.getPassword());
            stm.setInt(5, chef.getRestaurant().getId());
            stm.setInt(6, chef.getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (check != 0);
    }

    @Override
    public boolean delete(Chef chef) {
        int check = 0;
        try {
            PreparedStatement stm = connection.prepareStatement(
                    "DELETE FROM chef WHERE chef_id = ?");
            stm.setInt(1, chef.getId());

            check = stm.executeUpdate();

            stm.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return (check != 0);
    }
}
