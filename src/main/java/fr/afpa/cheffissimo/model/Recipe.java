package fr.afpa.cheffissimo.model;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

// import fr.afpa.cheffissimo.model.DAO.ComponentDAO;
// import fr.afpa.cheffissimo.util.TimeUtil;

public class Recipe {
    private int id;
    private String name;
    private Time totalTime;
    private int servingsNb;
    private String type;
    private boolean vegeBool;
    private float rating;
    private String[] steps;
    private Chef chef;

    public Recipe() {
    }

    public Recipe(String name, Time totalTime, int servingsNb, String type, boolean vegeBool, float rating,
            String[] steps, Chef chef) {
        this.name = name;
        this.totalTime = totalTime;
        this.servingsNb = servingsNb;
        this.type = type;
        this.vegeBool = vegeBool;
        this.rating = rating;
        this.steps = steps;
        this.chef = chef;
    }

    public Recipe(int id, String name, Time totalTime, int servingsNb, String type, boolean vegeBool, float rating,
            String[] steps, Chef chef) {
        this.id = id;
        this.name = name;
        this.totalTime = totalTime;
        this.servingsNb = servingsNb;
        this.type = type;
        this.vegeBool = vegeBool;
        this.rating = rating;
        this.steps = steps;
        this.chef = chef;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Time getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Time totalTime) {
        this.totalTime = totalTime;
    }

    public int getServingsNb() {
        return servingsNb;
    }

    public void setServingsNb(int servingsNb) {
        this.servingsNb = servingsNb;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type.toUpperCase();
    }

    public boolean isVegeBool() {
        return vegeBool;
    }

    public void setVegeBool(boolean vegeBool) {
        this.vegeBool = vegeBool;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String[] getSteps() {
        return steps;
    }

    public void setSteps(String[] steps) {
        this.steps = steps;
    }

    public Chef getChef() {
        return chef;
    }

    public void setChef(Chef chef) {
        this.chef = chef;
    }

    @Override
    public boolean equals(Object arg0) {
        Recipe recipe = (Recipe) arg0;
        return (this.name.equals(recipe.name));
    }

    @Override
    public String toString() {
        return "Recipe [id=" + id + ", name=" + name + ", totalTime=" + totalTime + ", servingsNb=" + servingsNb
                + ", type=" + type + ", vegeBool=" + vegeBool + ", chef=" + chef + "]";
    }

    public void calculateRating(List<Comment> comments) {
        float mean = 0;
        List<Integer> ratings = new ArrayList<>();
        for (Comment c : comments) {
            ratings.add(c.getRating());
        }
        mean = (float) ratings.stream().mapToDouble(f -> f).average().orElse(0.0);
        rating = mean;
    }

    // public String formatRecipe() {
    // componentDAO = new ComponentDAO();

    // String output = "";
    // String vege = "";
    // int hourInt = TimeUtil.formatTimeToHashMap(totalTime).get("hour");
    // String hourString = "";
    // int minuteInt = TimeUtil.formatTimeToHashMap(totalTime).get("minute");
    // String minuteString = "";

    // if(isVegeBool()) vege = " - végétarien";
    // if(hourInt > 0) hourString = Integer.toString(hourInt) + "h ";
    // if(minuteInt > 0) minuteString = Integer.toString(minuteInt) + "min";

    // output += "Nom de la recette : " + name + vege + "\n";
    // output += "Temps de préparation : " + hourString + minuteString + "\n";
    // output += "Nombre de couverts : " + servingsNb + "\n";
    // output += "Créateur : " + chef + "\n";
    // output += "\n";
    // output += "Ingrédients :\n";
    // for(Component c : componentDAO.getAllByRecipe(this)){
    // output += " " + c.getName() + " : " + c.getQuantity() + c.getUnit() + "\n";
    // }
    // output += "\n";
    // output += "Préparation :\n";
    // for(int i = 0 ; i < steps.length ; i++){
    // output += (i + 1) + ". " + steps[i] + "\n";
    // }
    // return output;
    // }
}
