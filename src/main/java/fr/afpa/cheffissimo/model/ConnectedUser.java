package fr.afpa.cheffissimo.model;

public final class ConnectedUser {
    private static ConnectedUser instance;
    private Chef chef;
    private String errorMessage;

    private ConnectedUser() {
    }

    public static ConnectedUser getInstance() {
        if (instance == null) {
            instance = new ConnectedUser();
        }
        return instance;
    }

    public Chef getChef() {
        return chef;
    }

    public void setChef(Chef chef) {
        this.chef = chef;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
