package fr.afpa.cheffissimo.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionPostgreSQL {
    private static final String URL = "jdbc:postgresql://localhost:5432/cheffissimo";
    private static final String DATABASE_NAME = "postgres";
    private static final String MDP = "Unpetitpaspourlhomme";

    public static Connection connection;

    private ConnectionPostgreSQL() {

    }

    public static Connection getInstance() {
        try {
            connection = DriverManager.getConnection(URL, DATABASE_NAME, MDP);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
