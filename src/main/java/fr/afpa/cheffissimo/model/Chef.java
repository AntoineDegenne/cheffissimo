package fr.afpa.cheffissimo.model;

public class Chef {
    private int id;
    private String lastName;
    private String firstName;
    private String mail;
    private String password;
    private Restaurant restaurant;

    public Chef() {
    }

    public Chef(String lastName, String firstName, String mail, String password, Restaurant restaurant) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.mail = mail;
        this.password = password;
        this.restaurant = restaurant;
    }

    public Chef(int id, String lastName, String firstName, String mail, String password, Restaurant restaurant) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.mail = mail;
        this.password = password;
        this.restaurant = restaurant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName;
    }
}
