package fr.afpa.cheffissimo.model;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.cheffissimo.util.FloatUtil;
import fr.afpa.cheffissimo.util.TimeUtil;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public final class RecipeData {
    private static RecipeData INSTANCE;
    private Recipe recipe;
    private List<Component> components = new ArrayList<>();
    private String origin;

    private RecipeData() {
    }

    public static RecipeData getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new RecipeData();
        }
        return INSTANCE;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public List<Component> getComponents() {
        return components;
    }

    public void setComponents(List<Component> components) {
        this.components = components;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void clear() {
        recipe = null;
        components = null;
    }

    public static TextFlow formatRecipe(Recipe recipe, List<Component> components) {

        Font titleFont = Font.font("Candara", FontWeight.BOLD, 20);
        Font contentFont = new Font("Candara", 16);
        TextFlow output = new TextFlow();

        Text title1 = new Text("Nom de la recette : ");
        Text title2 = new Text("Temps de préparation : ");
        Text title3 = new Text("Nombre de couverts : ");
        Text title4 = new Text("Créateur : ");
        Text title5 = new Text("Ingrédients :\n");
        Text title6 = new Text("Préparation :\n");
        List<Text> titles = List.of(title1, title2, title3, title4, title5, title6);
        for (Text text : titles) {

            text.setFont(titleFont);
        }

        String vege = "";
        int hourInt = TimeUtil.formatTimeToHashMap(recipe.getTotalTime()).get("hour");
        String hourString = "";
        int minuteInt = TimeUtil.formatTimeToHashMap(recipe.getTotalTime()).get("minute");
        String minuteString = "";

        if (recipe.isVegeBool())
            vege = "    - végétarien";
        if (hourInt > 0)
            hourString = String.format("%sh ", Integer.toString(hourInt));
        if (minuteInt > 0)
            minuteString = Integer.toString(minuteInt) + "min";

        Text content1 = new Text(recipe.getName());
        Text content11 = new Text(vege + "\n");
        content11.setFill(Color.FORESTGREEN);
        Text content2 = new Text(hourString + minuteString + "\n");
        Text content3 = new Text(recipe.getServingsNb() + "\n");
        Text content4 = new Text(recipe.getChef() + "\n\n");
        List<Text> contents = List.of(content1, content11, content2, content3, content4);
        for (Text text : contents) {
            text.setFont(contentFont);
        }

        List<Text> content5 = new ArrayList<>();
        for (Component c : components) {
            if (c.getName() != null && !c.getName().isEmpty()) {
                String quantity = (c.getQuantity() == 0) ? "Non spécifié" : FloatUtil.roundFloat(c.getQuantity());
                Text text = new Text("     " + c.getName() + " : " + quantity + c.getUnit() + "\n");
                text.setFont(contentFont);
                content5.add(text);
            }
        }
        content5.add(new Text("\n"));
        List<Text> content6 = new ArrayList<>();
        for (int i = 0; i < recipe.getSteps().length; i++) {
            Text text = new Text((i + 1) + ". " + recipe.getSteps()[i] + "\n");
            text.setFont(contentFont);
            content6.add(text);
        }

        TextFlow line1 = new TextFlow(titles.get(0), contents.get(0), contents.get(1));
        TextFlow line2 = new TextFlow(titles.get(1), contents.get(2));
        TextFlow line3 = new TextFlow(titles.get(2), contents.get(3));
        TextFlow line4 = new TextFlow(titles.get(3), contents.get(4));
        TextFlow line5 = new TextFlow(titles.get(4));

        output.getChildren().addAll(line1, new Text(System.lineSeparator()),
                line2, new Text(System.lineSeparator()),
                line3, new Text(System.lineSeparator()),
                line4, new Text(System.lineSeparator()),
                line5, new Text(System.lineSeparator()));
        output.getChildren().addAll(content5);
        output.getChildren().add(title6);
        output.getChildren().addAll(content6);
        output.setLineSpacing(5);
        return output;
    }
}
