package fr.afpa.cheffissimo.model;

public class Comment {
    private String content;
    private int rating;
    private String date;
    private Chef chef;
    private Recipe recipe;

    public Comment() {
    }

    public Comment(String content, int rating) {
        this.content = content;
        this.rating = rating;
    }

    public Comment(String content, int rating, Chef chef, Recipe recipe) {
        this.content = content;
        this.rating = rating;
        this.chef = chef;
        this.recipe = recipe;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public Chef getChef() {
        return chef;
    }

    public void setChef(Chef chef) {
        this.chef = chef;
    }

    public Recipe getRecipe() {
        return recipe;
    }

    public void setRecipe(Recipe recipe) {
        this.recipe = recipe;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Comment [content=" + content + ", rating=" + rating + ", chef=" + chef + ", recipe=" + recipe + "]";
    }
}
