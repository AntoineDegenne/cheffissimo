package fr.afpa.cheffissimo.controller;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.cheffissimo.App;
import fr.afpa.cheffissimo.model.Chef;
import fr.afpa.cheffissimo.model.Comment;
import fr.afpa.cheffissimo.model.ConnectedUser;
import fr.afpa.cheffissimo.model.Recipe;
import fr.afpa.cheffissimo.model.RecipeData;
import fr.afpa.cheffissimo.model.DAO.CommentDAO;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;

public class ViewCommentsController {
    private Chef connectedChef = ConnectedUser.getInstance().getChef();
    private RecipeData recipeData = RecipeData.getInstance();
    private Recipe recipeInput = recipeData.getRecipe();
    private CommentDAO commentDAO = new CommentDAO();

    private Comment commentInput;

    @FXML
    private Label mailLabel;

    @FXML
    private TableView<Comment> commentTableView;
    private ObservableList<Comment> commentObservableList = FXCollections.observableArrayList();

    @FXML
    private TableColumn<Comment, String> commentContentColumn;
    @FXML
    private TableColumn<Comment, Comment> commentRatingColumn;
    @FXML
    private TableColumn<Comment, String> commentChefColumn;
    @FXML
    private TableColumn<Comment, String> commentDateColumn;

    @FXML
    private Button star1Button;
    @FXML
    private Button star2Button;
    @FXML
    private Button star3Button;
    @FXML
    private Button star4Button;
    @FXML
    private Button star5Button;
    private List<Button> starButtons = new ArrayList<>();

    @FXML
    private TextArea commentTextArea;

    @FXML
    void initialize() {
        starButtons.add(star1Button);
        starButtons.add(star2Button);
        starButtons.add(star3Button);
        starButtons.add(star4Button);
        starButtons.add(star5Button);
        // If it already exists, get the comment of the connected chef for the selected
        // recipe
        commentInput = commentDAO.getByChefAndRecipe(connectedChef, recipeInput);

        // Set user mail top right
        mailLabel.setText(connectedChef.getMail());
        // If the user already had a comment, set it in the textArea
        if (commentInput.getContent() != null) {
            commentTextArea.setText(commentInput.getContent());
        }
        if (commentInput.getRating() > 0) {
            setupRatingStars(commentInput.getRating());
        }

        commentObservableList.setAll(commentDAO.getAllByRecipe(recipeInput));

        commentTableView.setItems(commentObservableList);

        commentContentColumn
                .setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getContent()));
        setupRatingColumn();
        commentChefColumn
                .setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getChef().toString()));
        // Allows the comment column to wrap text
        commentContentColumn.setCellFactory(tc -> {
            TableCell<Comment, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            cell.setPrefHeight(Control.USE_COMPUTED_SIZE);
            text.wrappingWidthProperty().bind(commentContentColumn.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            return cell;
        });
        commentRatingColumn.setStyle("-fx-alignment: CENTER;");
        commentChefColumn.setStyle("-fx-alignment: CENTER;");
        commentDateColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDate()));

    }

    @FXML
    void saveComment() {

        if (commentInput.getRating() > 0) {
            /*
             * If the comment already existed, update it, else save it
             */
            if (commentInput.getContent() == null) {
                // If the user left a blank content, keep comment null
                String contentInput = commentTextArea.getText();
                String content = (contentInput == null || contentInput.isBlank()) ? null : contentInput;

                commentInput.setContent(content);
                commentInput.setChef(connectedChef);
                commentInput.setRecipe(recipeInput);
                commentDAO.save(commentInput);
            } else {
                commentInput.setContent(commentTextArea.getText());
                commentDAO.update(commentInput);
            }
        }
        App.setRoot("viewRecipe");
    }

    @FXML
    void star1() {
        commentInput.setRating(1);
        setupRatingStars(1);
    }

    @FXML
    void star2() {
        commentInput.setRating(2);
        setupRatingStars(2);
    }

    @FXML
    void star3() {
        commentInput.setRating(3);
        setupRatingStars(3);
    }

    @FXML
    void star4() {
        commentInput.setRating(4);
        setupRatingStars(4);
    }

    @FXML
    void star5() {
        commentInput.setRating(5);
        setupRatingStars(5);
    }

    @FXML
    void back() {
        App.setRoot("viewRecipe");

    }

    private void setupRatingStars(int rating) {
        for (int i = 0; i < rating; i++) {
            ImageView starImageView = new ImageView(new Image(App.class.getResourceAsStream("image/star.png")));
            starImageView.setFitHeight(40);
            starImageView.setFitWidth(40);
            starButtons.get(i).setGraphic(starImageView);
        }
        for (int i = 4; i > rating - 1; i--) {
            ImageView starImageView = new ImageView(new Image(App.class.getResourceAsStream("image/empty_star.png")));
            starImageView.setFitHeight(40);
            starImageView.setFitWidth(40);
            starButtons.get(i).setGraphic(starImageView);
        }
    }

    private void setupRatingColumn() {
        // just use whole row (User) as data for cells in this column:
        commentRatingColumn.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue()));

        // cell factory for toggle buttons:
        commentRatingColumn.setCellFactory(c -> new TableCell<Comment, Comment>() {

            private final StackPane button = new StackPane();

            // Just update graphic as needed:

            @Override
            public void updateItem(Comment item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setGraphic(null);
                } else {
                    ImageView ratingStars = new ImageView(new Image(App.class.getResourceAsStream("image/rating.png")));
                    ratingStars.setFitWidth(100);
                    ratingStars.setFitHeight(18);
                    ProgressBar progressBar = new ProgressBar((double) item.getRating() / 5);
                    progressBar.setStyle(
                            "-fx-background-color: transparent; -fx-box-border: transparent; -fx-accent: yellow");
                    button.getChildren().addAll(progressBar, ratingStars);
                    button.setAlignment(Pos.CENTER);
                    setGraphic(button);
                }
            }
        });
    }

}
