package fr.afpa.cheffissimo.controller;

import java.util.ArrayList;
import java.util.List;

import fr.afpa.cheffissimo.App;
import fr.afpa.cheffissimo.model.Chef;
import fr.afpa.cheffissimo.model.Component;
import fr.afpa.cheffissimo.model.ConnectedUser;
import fr.afpa.cheffissimo.model.Recipe;
import fr.afpa.cheffissimo.model.RecipeData;
import fr.afpa.cheffissimo.util.PDFUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.TextFlow;

public class ViewRecipeController {

    private Chef connectedChef = ConnectedUser.getInstance().getChef();
    private RecipeData recipeData = RecipeData.getInstance();
    private Recipe recipeInput = recipeData.getRecipe();
    private List<Component> componentsInput = recipeData.getComponents();

    @FXML
    private Label mailLabel;

    @FXML
    private Label nameLabel;
    @FXML
    private Label chefLabel;
    @FXML
    private ImageView ratingImageView;
    @FXML
    private AnchorPane textPane;
    @FXML
    private Label servingsNbLabel;
    @FXML
    private Button minusButton;
    @FXML
    private Button minusTenButton;
    @FXML
    private Button plusButton;
    @FXML
    private Button plusTenButton;

    @FXML
    private Button historyButton;
    @FXML
    private Button commentsButton;
    @FXML
    private Button exportButton;

    @FXML
    private ProgressBar ratingProgressBar;

    @FXML
    private TextFlow flow;

    @FXML
    void initialize() {
        mailLabel.setText(connectedChef.getMail());
        ratingProgressBar.setProgress(recipeInput.getRating() / 5);

        nameLabel.setText(recipeInput.getName());
        chefLabel.setText(recipeInput.getChef().toString());

        textPane.getChildren().add(RecipeData.formatRecipe(recipeInput, componentsInput));
        servingsNbLabel.setText(Integer.toString(recipeInput.getServingsNb()));
    }

    @FXML
    void openHistory() {
        App.setRoot("viewHistory");
    }

    @FXML
    void openComments() {
        App.setRoot("viewComments");
    }

    @FXML
    void minus() {
        changeServingsNb(-1);
    }

    @FXML
    void minusTen() {
        changeServingsNb(-10);
    }

    @FXML
    void plus() {
        changeServingsNb(1);
    }

    @FXML
    void plusTen() {
        changeServingsNb(10);
    }

    @FXML
    void exportToPDF() {
        PDFUtil.exportTextFlowToPDF((TextFlow) textPane.getChildren().get(0));
    }

    @FXML
    void back() {
        recipeData.clear();
        App.setRoot(recipeData.getOrigin());
    }

    private void changeServingsNb(int nb) {
        int oldServingsNumber = recipeInput.getServingsNb();
        int newServingsNb = recipeInput.getServingsNb() + nb;
        if (newServingsNb > 0) {
            List<Component> newComponents = new ArrayList<>();
            for (Component c : componentsInput) {
                c.setQuantity(c.getQuantity() * ((float) newServingsNb / oldServingsNumber));
                newComponents.add(c);
            }
            recipeData.setComponents(newComponents);
            recipeInput.setServingsNb(newServingsNb);
            textPane.getChildren().set(0, RecipeData.formatRecipe(recipeInput, newComponents));
            servingsNbLabel.setText(Integer.toString(recipeInput.getServingsNb()));
        }
    }
}
