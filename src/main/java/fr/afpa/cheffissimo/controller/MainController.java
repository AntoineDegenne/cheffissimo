package fr.afpa.cheffissimo.controller;

import java.util.Collections;

import fr.afpa.cheffissimo.App;
import fr.afpa.cheffissimo.model.Chef;
import fr.afpa.cheffissimo.model.ConnectedUser;
import fr.afpa.cheffissimo.model.Recipe;
import fr.afpa.cheffissimo.model.RecipeData;
import fr.afpa.cheffissimo.model.DAO.ComponentDAO;
import fr.afpa.cheffissimo.model.DAO.RecipeDAO;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;

public class MainController {

    private Chef connectedChef = ConnectedUser.getInstance().getChef();
    private RecipeData recipeData = RecipeData.getInstance();
    private RecipeDAO recipeDAO = new RecipeDAO();

    @FXML
    private ComboBox<String> typeComboBox;
    private ObservableList<String> recipeTypeObservableList = FXCollections.observableArrayList("Tout", "Entree",
            "Plat",
            "Dessert");
    @FXML
    private CheckBox vegeCheckBox;

    @FXML
    private TableView<Recipe> recipesTableView;
    @FXML
    private TableColumn<Recipe, String> nameColumn;
    @FXML
    private TableColumn<Recipe, String> chefColumn;
    @FXML
    private TableColumn<Recipe, Recipe> ratingColumn;
    @FXML
    private TableColumn<Recipe, Recipe> buttonColumn;

    private ObservableList<Recipe> recipesObservableList = FXCollections.observableArrayList();

    @FXML
    private Button createRecipeButton;
    @FXML
    private Button chefRecipesButton;

    @FXML
    private TextField searchField;

    @FXML
    private Label mailLabel;

    @FXML
    void initialize() {
        // Set user mail top right
        mailLabel.setText(connectedChef.getMail());

        vegeCheckBox.setSelected(false);

        FilteredList<Recipe> filteredRecipes = new FilteredList<>(recipesObservableList, p -> true);

        // Manage ComboBox to select type and show the right values in main tableView
        typeComboBox.setItems(recipeTypeObservableList);
        typeComboBox.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal) -> {
            if (newVal.equals("Tout")) {
                recipesObservableList.setAll(recipeDAO.getAll());
                Collections.reverse(recipesObservableList);
                recipesTableView.refresh();
            } else if (newVal != null) {
                recipesObservableList.setAll(recipeDAO.getAllByType(newVal.toUpperCase()));
                Collections.reverse(recipesObservableList);
                recipesTableView.refresh();
            }
        });
        typeComboBox.getSelectionModel().select(0);

        // Initialize the columns
        nameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        chefColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getChef().toString()));
        setupRatingColumn();
        setupButtonColumn();

        searchField.textProperty().addListener((obs, oldValue, newValue) -> {
            filteredRecipes.setPredicate(p -> (p.getName().toLowerCase().contains(newValue.toLowerCase().trim())));
        });
        vegeCheckBox.selectedProperty().addListener((obs, oldValue, newValue) -> {
            filteredRecipes.setPredicate(p -> (p.isVegeBool() == vegeCheckBox.isSelected() || p.isVegeBool() == true));
        });

        recipesTableView.setItems(filteredRecipes);
    }

    @FXML
    void openChefRecipes() {
        recipeData.setOrigin("main");
        App.setRoot("viewOwnerRecipes");
    }

    @FXML
    void openCreateRecipe() {
        recipeData.setOrigin("main");
        App.setRoot("editRecipe");
    }

    private void setupButtonColumn() {
        buttonColumn.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue()));

        buttonColumn.setCellFactory(c -> new TableCell<Recipe, Recipe>() {

            // create toggle button once for cell:
            private final Button button = new Button("Voir la recette");

            // anonymous constructor:
            {
                button.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
                    ComponentDAO componentDAO = new ComponentDAO();
                    recipeData.setRecipe(getItem());
                    recipeData.setComponents(componentDAO.getAllByRecipe(getItem()));
                    recipeData.setOrigin("main");
                    App.setRoot("viewRecipe");
                });
            }

            // Just update graphic as needed:

            @Override
            public void updateItem(Recipe item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setGraphic(null);
                } else {
                    setGraphic(button);
                }
            }
        });
    }

    private void setupRatingColumn() {
        // just use whole row (User) as data for cells in this column:
        ratingColumn.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue()));

        // cell factory for toggle buttons:
        ratingColumn.setCellFactory(c -> new TableCell<Recipe, Recipe>() {

            // create toggle button once for cell:
            private final StackPane button = new StackPane();

            // Just update graphic as needed:

            @Override
            public void updateItem(Recipe item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setGraphic(null);
                } else {
                    ImageView ratingStars = new ImageView(new Image(App.class.getResourceAsStream("image/rating.png")));
                    ratingStars.setFitWidth(100);
                    ratingStars.setFitHeight(18);
                    ProgressBar progressBar = new ProgressBar((double) item.getRating() / 5);
                    progressBar.setStyle(
                            "-fx-background-color: transparent; -fx-box-border: transparent; -fx-accent: yellow");
                    button.getChildren().addAll(progressBar, ratingStars);
                    button.setAlignment(Pos.CENTER);
                    setGraphic(button);
                }
            }
        });
    }

}
