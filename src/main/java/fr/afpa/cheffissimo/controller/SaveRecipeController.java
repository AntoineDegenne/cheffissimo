package fr.afpa.cheffissimo.controller;

import fr.afpa.cheffissimo.App;
import fr.afpa.cheffissimo.model.Component;
import fr.afpa.cheffissimo.model.Recipe;
import fr.afpa.cheffissimo.model.RecipeData;
import fr.afpa.cheffissimo.model.DAO.ComponentDAO;
import fr.afpa.cheffissimo.model.DAO.RecipeDAO;
import fr.afpa.cheffissimo.util.PDFUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.TextFlow;

public class SaveRecipeController {
    private RecipeData recipeData = RecipeData.getInstance();
    private Recipe recipeInput = recipeData.getRecipe();
    private RecipeDAO recipeDAO;
    private ComponentDAO componentDAO;

    @FXML
    private Button backButton;
    @FXML
    private AnchorPane textPane;
    @FXML
    private Button exportButton;
    @FXML
    private Button saveButton;
    @FXML
    private Label mailLabel;

    @FXML
    void initialize() {
        recipeDAO = new RecipeDAO();
        componentDAO = new ComponentDAO();
        textPane.getChildren().add(RecipeData.formatRecipe(recipeInput, recipeData.getComponents()));
    }

    @FXML
    void back() {
        App.setRoot("editRecipe");
    }

    @FXML
    void save() {
        // Si la recette a déjà un id, on update
        if (recipeInput.getId() > 0) {
            // Si l'update réussi, on update les ingrédients
            if (recipeDAO.update(recipeInput)) {
                for (Component c : recipeData.getComponents()) {
                    // Si l'ingrédient à déjà un id, on l'update
                    if (c.getId() > 0) {
                        // Si l'ingrédient a été supprimé, on le delete
                        if (c.getName() == null || c.getName().isEmpty()) {
                            componentDAO.delete(c);
                        }
                        componentDAO.update(c);
                        // Sinon, on l'ajoute
                    } else {
                        componentDAO.save(c);
                    }
                }
                recipeData.clear();
                App.setRoot("main");
            }
            // Si la racette n'as pas d'id, on l'ajoute
        } else {
            if (recipeDAO.save(recipeInput)) {
                // et on ajoute chaque ingédient
                for (Component c : recipeData.getComponents()) {
                    if (c.getName() != null) {
                        componentDAO.save(c);
                    }
                }
                recipeData.clear();
                App.setRoot("main");
            }
        }
    }

    @FXML
    void exportToPDF() {
        PDFUtil.exportTextFlowToPDF((TextFlow) textPane.getChildren().get(0));
    }
}
