package fr.afpa.cheffissimo.controller;

import java.util.Collections;
import java.util.Optional;

import fr.afpa.cheffissimo.App;
import fr.afpa.cheffissimo.model.Chef;
import fr.afpa.cheffissimo.model.ConnectedUser;
import fr.afpa.cheffissimo.model.Recipe;
import fr.afpa.cheffissimo.model.RecipeData;
import fr.afpa.cheffissimo.model.DAO.ComponentDAO;
import fr.afpa.cheffissimo.model.DAO.RecipeDAO;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.MouseEvent;

public class ViewOwnerRecipes {
    private Chef connectedChef = ConnectedUser.getInstance().getChef();
    private RecipeData recipeData = RecipeData.getInstance();
    private RecipeDAO recipeDAO = new RecipeDAO();

    @FXML
    private ComboBox<String> typeComboBox;
    private ObservableList<String> recipeTypeObservableList = FXCollections.observableArrayList("Tout", "Entree",
            "Plat",
            "Dessert");
    @FXML
    private CheckBox vegeCheckBox;

    @FXML
    private TableView<Recipe> recipesTableView;
    @FXML
    private TableColumn<Recipe, String> nameColumn;
    @FXML
    private TableColumn<Recipe, Recipe> viewButtonColumn;
    @FXML
    private TableColumn<Recipe, Recipe> editButtonColumn;
    @FXML
    private TableColumn<Recipe, Recipe> deleteButtonColumn;

    private ObservableList<Recipe> recipesObservableList = FXCollections.observableArrayList();

    @FXML
    private Button createRecipeButton;

    @FXML
    private TextField searchField;

    @FXML
    private Label mailLabel;

    @FXML
    void initialize() {
        // Set user mail top right
        mailLabel.setText(connectedChef.getMail());

        vegeCheckBox.setSelected(false);
        recipesObservableList.setAll(recipeDAO.getAllByChef(connectedChef));

        FilteredList<Recipe> filteredRecipes = new FilteredList<>(recipesObservableList, p -> true);

        // Manage ComboBox to select type and show the right values in main tableView
        typeComboBox.setItems(recipeTypeObservableList);
        typeComboBox.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal) -> {
            if (newVal.equals("Tout")) {
                recipesObservableList.setAll(recipeDAO.getAllByChef(connectedChef));
                Collections.reverse(recipesObservableList);
                recipesTableView.refresh();
            } else if (newVal != null) {
                recipesObservableList.setAll(recipeDAO.getAllByChefByType(connectedChef, newVal.toUpperCase()));
                Collections.reverse(recipesObservableList);
                recipesTableView.refresh();
            }
        });
        typeComboBox.getSelectionModel().select(0);

        // Initialize the columns
        nameColumn.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getName()));
        setupViewButtonColumn();
        setupEditButtonColumn();
        setupDeleteButtonColumn();

        searchField.textProperty().addListener((obs, oldValue, newValue) -> {
            filteredRecipes.setPredicate(p -> (p.getName().toLowerCase().contains(newValue.toLowerCase().trim())));
        });
        vegeCheckBox.selectedProperty().addListener((obs, oldValue, newValue) -> {
            filteredRecipes.setPredicate(p -> (p.isVegeBool() == vegeCheckBox.isSelected() || p.isVegeBool() == true));
        });

        recipesTableView.setItems(filteredRecipes);

    }

    @FXML
    void openCreateRecipe() {
        recipeData.setOrigin("viewOwnerRecipes");
        App.setRoot("editRecipe");
    }

    @FXML
    void back() {
        recipeData.clear();
        App.setRoot("main");
    }

    private void setupViewButtonColumn() {
        viewButtonColumn.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue()));

        viewButtonColumn.setCellFactory(c -> new TableCell<Recipe, Recipe>() {

            // create toggle button once for cell:
            private final Button button = new Button("Voir la recette");

            // anonymous constructor:
            {
                button.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
                    ComponentDAO componentDAO = new ComponentDAO();
                    recipeData.setRecipe(getItem());
                    recipeData.setComponents(componentDAO.getAllByRecipe(getItem()));
                    recipeData.setOrigin("viewOwnerRecipes");
                    App.setRoot("viewRecipe");
                });
            }

            // Just update graphic as needed:

            @Override
            public void updateItem(Recipe item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setGraphic(null);
                } else {
                    setGraphic(button);
                }
            }
        });
    }

    private void setupEditButtonColumn() {
        editButtonColumn.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue()));

        editButtonColumn.setCellFactory(c -> new TableCell<Recipe, Recipe>() {

            // create toggle button once for cell:
            private final Button button = new Button("Modifier la recette");

            // anonymous constructor:
            {
                button.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
                    ComponentDAO componentDAO = new ComponentDAO();
                    recipeData.setRecipe(getItem());
                    recipeData.setComponents(componentDAO.getAllByRecipe(getItem()));
                    recipeData.setOrigin("viewOwnerRecipes");
                    App.setRoot("editRecipe");
                });
            }

            // Just update graphic as needed:

            @Override
            public void updateItem(Recipe item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setGraphic(null);
                } else {
                    setGraphic(button);
                }
            }
        });
    }

    private void setupDeleteButtonColumn() {

        deleteButtonColumn.setCellValueFactory(cellData -> new ReadOnlyObjectWrapper<>(cellData.getValue()));

        deleteButtonColumn.setCellFactory(c -> new TableCell<Recipe, Recipe>() {

            // create toggle button once for cell:
            private final Button button = new Button("Supprimer la recette");

            // anonymous constructor:
            {
                button.setStyle("-fx-background-color: red; -fx-text-fill: white");
                button.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent e) -> {
                    Alert alert = new Alert(AlertType.CONFIRMATION);
                    alert.setTitle("Attention");
                    alert.setHeaderText("Supprimer la recette");
                    alert.setContentText("Etes-vous certain de vouloir supprimer la recette ?");

                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == ButtonType.OK) {
                        recipeDAO.delete(getItem());
                        recipesObservableList.remove(getIndex());
                        recipesTableView.refresh();
                    }
                });
            }

            // Just update graphic as needed:

            @Override
            public void updateItem(Recipe item, boolean empty) {
                super.updateItem(item, empty);
                if (empty) {
                    setGraphic(null);
                } else {
                    setGraphic(button);
                }
            }
        });
    }
}
