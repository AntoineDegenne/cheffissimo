package fr.afpa.cheffissimo.controller;

import fr.afpa.cheffissimo.App;
import fr.afpa.cheffissimo.model.Chef;
import fr.afpa.cheffissimo.model.ConnectedUser;
import fr.afpa.cheffissimo.model.DAO.ChefDAO;
import fr.afpa.cheffissimo.util.ImageUtil;
import fr.afpa.cheffissimo.util.TextFieldUtil;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

public class ConnectionController {
    private ChefDAO chefDAO = new ChefDAO();
    private ConnectedUser connectedUser = ConnectedUser.getInstance();

    @FXML
    private AnchorPane bgPane;

    @FXML
    private TextField mailField;
    @FXML
    private PasswordField passwordField;
    @FXML
    private TextField hiddenField;

    @FXML
    private Button loginButton;
    @FXML
    private Button debugButton;
    @FXML
    private Button showButton;

    @FXML
    private Label errorLabel;

    @FXML
    private void initialize() {
        ImageUtil.addBackground(bgPane);
        errorLabel.setText("");
        TextFieldUtil.managePasswordField(passwordField, hiddenField, showButton);
    }

    @FXML
    private void connect() {

        String mailInput = mailField.getText();
        String passwordInput = passwordField.getText();
        int chefId = chefDAO.checkCredentials(mailInput, passwordInput);

        if (chefId > 0) {
            Chef chefInput = chefDAO.getById(chefId);
            connectedUser.setChef(chefInput);
            App.setRoot("main");
        } else {
            mailField.clear();
            passwordField.clear();
            if (mailInput == null || mailInput.isBlank()) {
                errorLabel.setText("Veuillez renseigner une adresse mail.");
            } else {
                errorLabel.setText(connectedUser.getErrorMessage());
            }
        }
    }

    /*
     * Fonction debug pour entrer direct les bonnes valeurs dans les champs
     * Pas de possibilité d'avoir plusieurs user pour l'instant
     */
    @FXML
    private void debug() {
        mailField.setText("thierry.deveaux@afpa.fr");
        passwordField.setText("1234password");
    }
}
