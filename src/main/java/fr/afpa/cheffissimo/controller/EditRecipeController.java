package fr.afpa.cheffissimo.controller;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import fr.afpa.cheffissimo.App;
import fr.afpa.cheffissimo.model.Chef;
import fr.afpa.cheffissimo.model.Component;
import fr.afpa.cheffissimo.model.ConnectedUser;
import fr.afpa.cheffissimo.model.Recipe;
import fr.afpa.cheffissimo.model.RecipeData;
import fr.afpa.cheffissimo.util.FloatUtil;
import fr.afpa.cheffissimo.util.TextFieldUtil;
import fr.afpa.cheffissimo.util.TimeUtil;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

public class EditRecipeController {

    private Chef connectedChef = ConnectedUser.getInstance().getChef();
    private RecipeData recipeData = RecipeData.getInstance();
    private Recipe recipeInput = recipeData.getRecipe();
    private List<Component> componentsInput = recipeData.getComponents();

    @FXML
    private Button backButton;
    @FXML
    private Button confirmButton;

    @FXML
    private TextField recipeNameField;
    @FXML
    private TextField recipeTimeHourField;
    @FXML
    private TextField recipeTimeMinField;
    @FXML
    private TextField recipeServingsNbField;
    @FXML
    private ComboBox<String> recipeTypeComboBox;
    private ObservableList<String> recipeTypeObservableList = FXCollections.observableArrayList("Entree", "Plat",
            "Dessert");
    @FXML
    private CheckBox recipeVegeCheckBox;

    @FXML
    private TextField componentNameField;
    @FXML
    private TextField componentQuantityField;
    @FXML
    private TextField componentUnitField;
    @FXML
    private Button componentAddButton;
    @FXML
    private Button componentModifyButton;
    @FXML
    private Button componentDeleteButton;
    @FXML
    private ListView<Component> componentListView;
    private ObservableList<Component> componentObservableList = FXCollections.observableArrayList();

    @FXML
    private TextArea stepTextArea;
    @FXML
    private Button stepAddButton;
    @FXML
    private Button stepModifyButton;
    @FXML
    private Button stepDeleteButton;
    @FXML
    private ListView<String> stepListView;
    private ObservableList<String> stepObservableList = FXCollections.observableArrayList();

    @FXML
    private Label mailLabel;

    @FXML
    void initialize() {
        mailLabel.setText(connectedChef.getMail());
        TextFieldUtil.setTextLimitToDigitFld(recipeTimeHourField, 2);
        TextFieldUtil.setTextLimitToDigitFld(recipeTimeMinField, 2);
        TextFieldUtil.setTextLimitToDigitFld(recipeServingsNbField, 4);
        TextFieldUtil.setTextLimitToRealFld(componentQuantityField, 2);
        setupRecipe();
        manageListViewWrapText(stepListView);
        manageListViewsFillFields();
        recipeTypeComboBox.setItems(recipeTypeObservableList);
        componentListView.setItems(componentObservableList);
        stepListView.setItems(stepObservableList);
    }

    @FXML
    void addComponent() {
        String quantityInput = componentQuantityField.getText();
        if (!componentNameField.getText().isEmpty()) {
            String quantity = (quantityInput == null || quantityInput.isBlank()) ? "0" : quantityInput;
            Component newComponent = new Component(componentNameField.getText(),
                    Float.parseFloat(quantity), componentUnitField.getText(),
                    recipeData.getRecipe());
            componentObservableList.add(newComponent);
            componentsInput.add(newComponent);
            componentRefresh();
        }
    }

    @FXML
    void modifyComponent() {
        Component selectedComponent = componentListView.getSelectionModel().getSelectedItem();
        int selectedComponentIndex = componentListView.getSelectionModel().getSelectedIndex();
        if (selectedComponent != null) {
            String quantityInput = componentQuantityField.getText();
            if (!componentNameField.getText().isEmpty()) {
                String quantity = (quantityInput == null || quantityInput.isBlank()) ? "0" : quantityInput;
                Component newComponent = new Component(selectedComponent.getId(), componentNameField.getText(),
                        Float.parseFloat(quantity), componentUnitField.getText(),
                        recipeData.getRecipe());
                componentObservableList.set(selectedComponentIndex, newComponent);
                componentsInput.set(selectedComponentIndex, newComponent);
                componentRefresh();
            }
        }
    }

    @FXML
    void deleteComponent() {
        int selectedComponentIndex = componentListView.getSelectionModel().getSelectedIndex();

        componentObservableList.remove(selectedComponentIndex);
        componentsInput.get(selectedComponentIndex).setName(null);
        componentRefresh();
    }

    @FXML
    void addStep() {
        if (!stepTextArea.getText().isEmpty()) {
            stepObservableList.add(stepTextArea.getText());
            stepRefresh();
        }
    }

    @FXML
    void modifyStep() {
        int selectedStepIndex = stepListView.getSelectionModel().getSelectedIndex();
        String selectedStep = stepListView.getSelectionModel().getSelectedItem();
        if (selectedStep != null) {
            selectedStep = stepTextArea.getText();
            stepObservableList.set(selectedStepIndex, selectedStep);
            stepRefresh();
        }
    }

    @FXML
    void deleteStep() {
        int selectedStepIndex = stepListView.getSelectionModel().getSelectedIndex();
        stepObservableList.remove(selectedStepIndex);
        stepRefresh();
    }

    @FXML
    void confirm() {
        if (!recipeNameField.getText().isEmpty() &&
                !recipeServingsNbField.getText().isEmpty() &&
                recipeTypeComboBox.getSelectionModel().getSelectedItem() != null &&
                !componentListView.getItems().isEmpty() &&
                !stepListView.getItems().isEmpty()) {

            String hoursInput = recipeTimeHourField.getText();
            String minutesInput = recipeTimeMinField.getText();
            String hours = (hoursInput == null || hoursInput.isBlank()) ? "00" : hoursInput;
            String minutes = (minutesInput.isEmpty() || minutesInput == null) ? "00" : minutesInput;
            String[] steps = new String[stepObservableList.size()];
            for (int i = 0; i < steps.length; i++) {
                steps[i] = stepObservableList.get(i);
            }

            recipeInput.setName(recipeNameField.getText());
            recipeInput.setTotalTime(Time.valueOf(hours + ":" + minutes + ":00"));
            recipeInput.setServingsNb(Integer.parseInt(recipeServingsNbField.getText()));
            recipeInput.setType(recipeTypeComboBox.getSelectionModel().getSelectedItem());
            recipeInput.setVegeBool(recipeVegeCheckBox.isSelected());
            recipeInput.setSteps(steps);
            recipeInput.setChef(connectedChef);

            recipeData.setRecipe(recipeInput);

            for (Component c : componentsInput) {
                c.setRecipe(recipeInput);
            }
            recipeData.setComponents(componentsInput);

            App.setRoot("saveRecipe");
        }
    }

    @FXML
    void back() {
        recipeData.clear();
        App.setRoot(recipeData.getOrigin());
    }

    private void setupRecipe() {
        if (recipeInput != null) {

            recipeNameField.setText(recipeInput.getName());
            recipeTimeHourField
                    .setText(Integer.toString(TimeUtil.formatTimeToHashMap(recipeInput.getTotalTime()).get("hour")));
            recipeTimeMinField
                    .setText(Integer.toString(TimeUtil.formatTimeToHashMap(recipeInput.getTotalTime()).get("minute")));
            recipeServingsNbField.setText(Integer.toString(recipeInput.getServingsNb()));
            recipeTypeComboBox.getSelectionModel().select(recipeInput.getType());
            recipeVegeCheckBox.setSelected(recipeInput.isVegeBool());

            componentObservableList.addAll(componentsInput);
            stepObservableList.addAll(recipeInput.getSteps());
        } else {
            recipeInput = new Recipe();
            componentsInput = new ArrayList<>();
        }
    }

    private void componentRefresh() {
        componentListView.refresh();
        componentNameField.clear();
        componentQuantityField.clear();
        componentUnitField.clear();
    }

    private void stepRefresh() {
        stepListView.refresh();
        stepTextArea.clear();
    }

    private void manageListViewsFillFields() {
        componentListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Component>() {
            @Override
            public void changed(ObservableValue<? extends Component> observable, Component oldValue,
                    Component newValue) {
                if (newValue != null) {
                    componentNameField.setText(newValue.getName());
                    componentQuantityField
                            .setText((newValue.getQuantity() == 0) ? "" : FloatUtil.roundFloat(newValue.getQuantity()));
                    componentUnitField.setText((newValue.getUnit()));
                }
            }
        });
        stepListView.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (newValue != null) {
                    stepTextArea.setText(newValue);
                }
            }
        });
    }

    private void manageListViewWrapText(ListView<String> list) {
        list.setCellFactory(param -> new ListCell<String>() {
            @Override
            protected void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                if (empty || item == null) {
                    setGraphic(null);
                    setText(null);

                } else {

                    setMinWidth(param.getWidth());
                    setMaxWidth(param.getWidth());
                    setPrefWidth(param.getWidth());

                    setWrapText(true);

                    setText(item.toString());

                }
            }
        });
    }
}
