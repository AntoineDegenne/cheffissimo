package fr.afpa.cheffissimo;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;

    @Override
    public void start(Stage stage) throws IOException {
        scene = new Scene(loadFXML("connection"), 1280, 1024);
        stage.setScene(scene);
        stage.setMinWidth(1280);
        stage.setMinHeight(1024);
        stage.show();
    }

    public static void setRoot(String string) {
        try {
            scene.setRoot(loadFXML(string));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setRoot(FXMLLoader fxmlLoader) throws IOException {
        scene.setRoot(fxmlLoader.load());
    }

    public static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }
}