package fr.afpa.cheffissimo.util;

import fr.afpa.cheffissimo.App;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Pane;

public class ImageUtil {
    // Permet d'aller chercher les imageview pour les boutons
    public static ImageView getIcon(String name) {
        Image imageOk = new Image(App.class.getResourceAsStream("image/" + name + ".png"));
        ImageView imageview = new ImageView(imageOk);
        imageview.setFitHeight(16);
        imageview.setFitWidth(16);
        return imageview;
    }

    public static void addBackground(Pane pane) {
        Image bgImage = new Image(App.class.getResourceAsStream("image/connection_bg.jpg"));
        BackgroundSize backgroundSize = new BackgroundSize(1280, 854, false, false, false, false);
        Background background = new Background(new BackgroundImage(bgImage, BackgroundRepeat.ROUND,
                BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER, backgroundSize));
        pane.setBackground(background);
    }
}
