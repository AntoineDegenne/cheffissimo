package fr.afpa.cheffissimo.util;

import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;

public class TextFieldUtil {

    public static void setTextLimitToDigitFld(TextField field, int length) {
        field.setTextFormatter(new TextFormatter<Integer>(change -> {
            String newText = change.getControlNewText();
            if (newText.matches("\\d*") && newText.length() <= length) {
                return change;
            }
            return null;
        }));
    }

    public static void setTextLimitToRealFld(TextField field, int decimalNb) {
        field.setTextFormatter(new TextFormatter<Integer>(change -> {
            String newText = change.getControlNewText();
            if (newText.matches("^[0-9]*\\.?[0-9]{0," + decimalNb + "}")) {
                return change;
            }
            return null;
        }));
    }

    public static void managePasswordField(PasswordField passwordField, TextField textField, Button showButton) {
        textField.setManaged(false);
        textField.setVisible(false);

        textField.managedProperty().bind(showButton.pressedProperty());
        textField.visibleProperty().bind(showButton.pressedProperty());

        passwordField.managedProperty().bind(showButton.pressedProperty().not());
        passwordField.visibleProperty().bind(showButton.pressedProperty().not());

        textField.textProperty().bindBidirectional(passwordField.textProperty());

        showButton.setGraphic(ImageUtil.getIcon("eye"));
        showButton.setStyle("-fx-background-color : transparent");

        showButton.addEventHandler(MouseEvent.MOUSE_PRESSED, (MouseEvent e) -> {

            showButton.setGraphic(ImageUtil.getIcon("antieye"));
            showButton.setStyle("-fx-background-color : lightgray");
        });
        showButton.addEventHandler(MouseEvent.MOUSE_RELEASED, (MouseEvent e) -> {
            showButton.setGraphic(ImageUtil.getIcon("eye"));
            showButton.setStyle("-fx-background-color : transparent");
        });
    }
}
