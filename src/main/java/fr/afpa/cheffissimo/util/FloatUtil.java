package fr.afpa.cheffissimo.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class FloatUtil {

    /**
     *  Arrondi à 2 décimales
     * @param nb
     * 
     */
    public static String roundFloat(float nb) {
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setRoundingMode(RoundingMode.FLOOR);

        return df.format(nb);
    }
}
