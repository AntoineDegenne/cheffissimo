package fr.afpa.cheffissimo.util;

import java.sql.Time;
import java.util.Calendar;
import java.util.HashMap;

public class TimeUtil {
    public static HashMap<String, Integer> formatTimeToHashMap(Time time) {
        HashMap<String, Integer> timeMap = new HashMap<>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(time);
        timeMap.put("hour", calendar.get(Calendar.HOUR));
        timeMap.put("minute", calendar.get(Calendar.MINUTE));
        timeMap.put("second", calendar.get(Calendar.SECOND));
        return timeMap;
    }
}
