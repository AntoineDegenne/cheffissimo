package fr.afpa.cheffissimo.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import javafx.scene.Node;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.FileChooser;

public class PDFUtil {
    public static void exportTextFlowToPDF(TextFlow textFlow) {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save PDF File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PDF Files", "*.pdf"),
                new FileChooser.ExtensionFilter("All Files", "*.*"));
        File outputFile = fileChooser.showSaveDialog(textFlow.getScene().getWindow());
        if (outputFile == null) {
            return;
        }

        Document document = new Document();
        try {
            PdfWriter.getInstance(document, new FileOutputStream(outputFile));
        } catch (FileNotFoundException | DocumentException e) {
            e.printStackTrace();
        }
        document.open();

        for (Node node : textFlow.getChildren()) {
            Paragraph paragraph = new Paragraph();
            if (node instanceof TextFlow) {
                for (Node nodule : ((TextFlow) node).getChildren()) {
                    Text text = (Text) nodule;
                    paragraph.add(text.getText());

                }
            } else if (node instanceof Text) {
                Text text = (Text) node;
                paragraph.add(text.getText());
            }
            paragraph.setAlignment(Element.ALIGN_LEFT);
            try {
                document.add(paragraph);
            } catch (DocumentException e) {
                e.printStackTrace();
            }
        }

        document.close();

    }
}
