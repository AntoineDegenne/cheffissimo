module fr.afpa.cheffissimo {
    requires transitive javafx.controls;
    requires transitive javafx.fxml;
    requires org.controlsfx.controls;
    requires transitive java.sql;
    requires itextpdf;

    opens fr.afpa.cheffissimo to javafx.fxml, java.sql;
    opens fr.afpa.cheffissimo.model to javafx.fxml, java.sql;
    opens fr.afpa.cheffissimo.model.DAO to javafx.fxml, java.sql;
    opens fr.afpa.cheffissimo.controller to javafx.fxml, java.sql, org.controlsfx.controls;
    opens fr.afpa.cheffissimo.util to javafx.fxml, org.controlsfx.controls, itextpdf;

    exports fr.afpa.cheffissimo;
    exports fr.afpa.cheffissimo.model;
    exports fr.afpa.cheffissimo.model.DAO;
    exports fr.afpa.cheffissimo.controller;
    exports fr.afpa.cheffissimo.util;
}
