package fr.afpa.cheffissimo.model.DAO;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class UserDAOTest {

    private static ChefDAO userDAO;

    @BeforeAll
    static void start() {
        userDAO = new ChefDAO();
    }

    @Test
    void testCheckCredentials() {
        assertTrue(userDAO.checkCredentials("paul.baucuse@villedelyon.fr", "1234paulthebest") > 0);
    }
}
