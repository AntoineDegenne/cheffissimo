package fr.afpa.cheffissimo.model.DAO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import fr.afpa.cheffissimo.model.Restaurant;

@TestMethodOrder(OrderAnnotation.class)
public class RestaurantDAOTest {

    private static RestaurantDAO restaurantDAO;
    private static Restaurant restauTest;

    @BeforeAll
    static void start() {
        restaurantDAO = new RestaurantDAO();
        restauTest = new Restaurant("test", "test", "test", "test", "test");
    }

    @Test
    void testGetById() {
        
        Restaurant restaurantExpected = new Restaurant(1, "Le Délicieux", null, null, null, null);
        Restaurant restaurantActual = restaurantDAO.getById(1);
        assertEquals(restaurantExpected, restaurantActual);
    }

    @Test
    @Order(1)
    void testSave() {
        assertTrue(restaurantDAO.save(restauTest));
    }

    @Test
    @Order(2)
    void testUpdate() {
        restauTest.setAddress("test2");
        assertTrue(restaurantDAO.update(restauTest));
    }

    @Test
    @Order(3)
    void testDelete() {
        assertTrue(restaurantDAO.delete(restauTest));
    }
}
