package fr.afpa.cheffissimo.model.DAO;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.sql.Time;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

import fr.afpa.cheffissimo.model.Recipe;
import fr.afpa.cheffissimo.model.Chef;

@TestMethodOrder(OrderAnnotation.class)
public class RecipeDAOTest {

    private static RecipeDAO recipeDAO;
    private static Recipe recipeTest;
    private static String[] testSteps = new String[1];

    @BeforeAll
    static void start() {
        recipeDAO = new RecipeDAO();
        Chef userTest = new Chef(1, null, null, null, null, null);
        recipeTest = new Recipe("test", Time.valueOf("1:30:00"), 4, "DESSERT", false, 3.2f, testSteps, userTest);
    }

    @Test
    @Order(1)
    void testSave() {
        assertTrue(recipeDAO.save(recipeTest));
    }

    @Test
    @Order(2)
    void testGetById() {
        assertEquals(recipeTest, recipeDAO.getById(recipeTest.getId()));
    }
    @Test
    @Order(3)
    void testUpdate() {
        recipeTest.setName("test2");
        assertTrue(recipeDAO.update(recipeTest));
    }

    @Test
    @Order(4)
    void testDelete() {
        assertTrue(recipeDAO.delete(recipeTest));
    }
}
