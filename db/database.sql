--
-- PostgreSQL database dump
--

-- Dumped from database version 15.2
-- Dumped by pg_dump version 15.2

-- Started on 2023-05-11 11:38:51

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 17588)
-- Name: public; Type: SCHEMA; Schema: -; Owner: pg_database_owner
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO pg_database_owner;

--
-- TOC entry 3430 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: pg_database_owner
--

COMMENT ON SCHEMA public IS 'standard public schema';


--
-- TOC entry 237 (class 1255 OID 17589)
-- Name: comment_changed(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.comment_changed() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
    NEW.update_time := DATE_TRUNC('second', NOW()) ;
  RETURN NEW;
END
$$;


ALTER FUNCTION public.comment_changed() OWNER TO postgres;

--
-- TOC entry 248 (class 1255 OID 17590)
-- Name: delete_components(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.delete_components() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
begin
	delete from component c where c.recipe_id = old.recipe_id;
	delete from public.recipe_audit ra where ra.recipe_id  = old.recipe_id;
	delete from "comment" com where com.recipe_id = old.recipe_id;
	return old;
END
$$;


ALTER FUNCTION public.delete_components() OWNER TO postgres;

--
-- TOC entry 245 (class 1255 OID 17591)
-- Name: hash_password(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.hash_password() RETURNS trigger
    LANGUAGE plpgsql
    AS $$

begin
	update "chef"
	set "password" = encode(digest(old."password", 'sha1'), 'hex')
	where chef_id = old.chef_id;
	return new;
END
$$;


ALTER FUNCTION public.hash_password() OWNER TO postgres;

--
-- TOC entry 246 (class 1255 OID 17592)
-- Name: hash_password(integer, text); Type: PROCEDURE; Schema: public; Owner: postgres
--

CREATE PROCEDURE public.hash_password(IN user_id integer, IN password_input text)
    LANGUAGE plpgsql
    AS $$

begin
	update "chef"
	set "password" = encode(digest(password_input, 'sha1'), 'hex')
	where chef_id = user_id;
END
$$;


ALTER PROCEDURE public.hash_password(IN user_id integer, IN password_input text) OWNER TO postgres;

--
-- TOC entry 279 (class 1255 OID 17593)
-- Name: recipe_audit_insert(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.recipe_audit_insert() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare names varchar(50)[];
declare quantities float4[];
declare units varchar(25)[];
BEGIN
  SELECT ARRAY(SELECT "name" FROM component where recipe_id = new.recipe_id) INTO names;
	SELECT ARRAY(SELECT quantity FROM component where recipe_id = new.recipe_id) INTO quantities;
	SELECT ARRAY(SELECT unit FROM component where recipe_id = new.recipe_id) INTO units;
  	INSERT INTO recipe_audit (recipe_id, event_type, timestamp, name, total_time, servings_nb, "type", vege_bool, chef_id, steps, components_names, components_quantities, components_units)
  	VALUES (new.recipe_id, 'INSERT', NOW(), new.name, new.total_time, new.servings_nb, new."type", new.vege_bool, new.chef_id, new.steps, names, quantities, units);
  RETURN NEW;
END;
$$;


ALTER FUNCTION public.recipe_audit_insert() OWNER TO postgres;

--
-- TOC entry 247 (class 1255 OID 17594)
-- Name: recipe_audit_update(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.recipe_audit_update() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
declare names varchar(50)[];
declare quantities float4[];
declare units varchar(25)[];
begin
	SELECT ARRAY(SELECT "name" FROM component where recipe_id = old.recipe_id) INTO names;
	SELECT ARRAY(SELECT quantity FROM component where recipe_id = old.recipe_id) INTO quantities;
	SELECT ARRAY(SELECT unit FROM component where recipe_id = old.recipe_id) INTO units;
  	INSERT INTO recipe_audit (recipe_id, event_type, timestamp, name, total_time, servings_nb, "type", vege_bool, chef_id, steps, components_names, components_quantities, components_units)
  	VALUES (OLD.recipe_id, 'UPDATE', NOW(), old.name, old.total_time, old.servings_nb, old."type", old.vege_bool, old.chef_id, old.steps, names, quantities, units);
 	 RETURN NEW;
END;
$$;


ALTER FUNCTION public.recipe_audit_update() OWNER TO postgres;

--
-- TOC entry 278 (class 1255 OID 17595)
-- Name: user_connection(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.user_connection(user_email text, user_password text) RETURNS integer
    LANGUAGE plpgsql
    AS $$
declare
user_id_reference int; -- l'identifiant de l'utilisateur récupéré en base de données
user_password_reference text; -- le mot de passe de l'utilisateur récupéré en base de données
user_exists boolean; -- un indicateur d'existence de l'utilisateur
hashed_password text; -- va contenir le mot de passe haché
begin
-- vérification de l'existence de l'utilisateur
user_exists = exists(select *
from chef c
where c.mail like user_email);
-- si l'utilisateur existe, on vérifie son mot de passe
if user_exists then
	-- récupération du mot de passe stocké en BDD
	select "password"
	into user_password_reference
	from chef c
	where c.mail like user_email;
	-- calcul du hash du mot de passe passé en paramètre et vérification avec le hash en BDD
	hashed_password = encode(digest(user_password, 'sha1'), 'hex');
	if hashed_password like user_password_reference then
		select chef_id
		into user_id_reference
		from chef c
		where c.mail like user_email;
		return user_id_reference;
	else
		raise notice 'Mot de passe incorrect.';
		return 0;
	end if;
else
	-- alert pour l'utilisateur
	raise notice 'L''utilisateur ayant pour email % n''existe pas en base de données.',
	user_email;
end if;
return 0;
END
$$;


ALTER FUNCTION public.user_connection(user_email text, user_password text) OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 215 (class 1259 OID 17596)
-- Name: chef; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chef (
    chef_id integer NOT NULL,
    last_name character varying(25),
    first_name character varying(25),
    mail character varying(50) NOT NULL,
    password character varying(100) NOT NULL,
    restaurant_id integer NOT NULL
);


ALTER TABLE public.chef OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 17599)
-- Name: comment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.comment (
    chef_id integer NOT NULL,
    recipe_id integer NOT NULL,
    content text,
    rating smallint,
    update_time timestamp without time zone
);


ALTER TABLE public.comment OWNER TO postgres;

--
-- TOC entry 217 (class 1259 OID 17604)
-- Name: component; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.component (
    component_id integer NOT NULL,
    name character varying(50) NOT NULL,
    quantity real,
    unit character varying(25),
    recipe_id integer NOT NULL
);


ALTER TABLE public.component OWNER TO postgres;

--
-- TOC entry 218 (class 1259 OID 17607)
-- Name: component_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.component_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.component_id_seq OWNER TO postgres;

--
-- TOC entry 3432 (class 0 OID 0)
-- Dependencies: 218
-- Name: component_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.component_id_seq OWNED BY public.component.component_id;


--
-- TOC entry 219 (class 1259 OID 17608)
-- Name: recipe; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipe (
    recipe_id integer NOT NULL,
    name character varying(50) NOT NULL,
    total_time time without time zone,
    servings_nb integer,
    type character varying(10),
    vege_bool boolean,
    chef_id integer NOT NULL,
    steps text[],
    CONSTRAINT check_type CHECK ((((type)::text ~~ 'ENTREE'::text) OR ((type)::text ~~ 'PLAT'::text) OR ((type)::text ~~ 'DESSERT'::text)))
);


ALTER TABLE public.recipe OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 17614)
-- Name: recipe_audit; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.recipe_audit (
    recipe_audit_id integer NOT NULL,
    recipe_id integer NOT NULL,
    event_type text NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    name character varying(50) NOT NULL,
    total_time time without time zone,
    servings_nb integer,
    type character varying(10),
    vege_bool boolean,
    chef_id integer NOT NULL,
    steps text[],
    components_names character varying[],
    components_quantities real[],
    components_units character varying[],
    CONSTRAINT check_type CHECK ((((type)::text ~~ 'ENTREE'::text) OR ((type)::text ~~ 'PLAT'::text) OR ((type)::text ~~ 'DESSERT'::text)))
);


ALTER TABLE public.recipe_audit OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 17620)
-- Name: recipe_audit_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recipe_audit_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipe_audit_id_seq OWNER TO postgres;

--
-- TOC entry 3433 (class 0 OID 0)
-- Dependencies: 221
-- Name: recipe_audit_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recipe_audit_id_seq OWNED BY public.recipe_audit.recipe_audit_id;


--
-- TOC entry 222 (class 1259 OID 17621)
-- Name: recipe_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.recipe_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recipe_id_seq OWNER TO postgres;

--
-- TOC entry 3434 (class 0 OID 0)
-- Dependencies: 222
-- Name: recipe_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.recipe_id_seq OWNED BY public.recipe.recipe_id;


--
-- TOC entry 223 (class 1259 OID 17622)
-- Name: restaurant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.restaurant (
    restaurant_id integer NOT NULL,
    name character varying(50) NOT NULL,
    address character varying(100),
    city character varying(50),
    phone character varying(20),
    mail character varying(50)
);


ALTER TABLE public.restaurant OWNER TO postgres;

--
-- TOC entry 224 (class 1259 OID 17625)
-- Name: restaurant_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.restaurant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.restaurant_id_seq OWNER TO postgres;

--
-- TOC entry 3435 (class 0 OID 0)
-- Dependencies: 224
-- Name: restaurant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.restaurant_id_seq OWNED BY public.restaurant.restaurant_id;


--
-- TOC entry 225 (class 1259 OID 17626)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- TOC entry 3436 (class 0 OID 0)
-- Dependencies: 225
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public.chef.chef_id;


--
-- TOC entry 3241 (class 2604 OID 17627)
-- Name: chef chef_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chef ALTER COLUMN chef_id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- TOC entry 3242 (class 2604 OID 17628)
-- Name: component component_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.component ALTER COLUMN component_id SET DEFAULT nextval('public.component_id_seq'::regclass);


--
-- TOC entry 3243 (class 2604 OID 17629)
-- Name: recipe recipe_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe ALTER COLUMN recipe_id SET DEFAULT nextval('public.recipe_id_seq'::regclass);


--
-- TOC entry 3244 (class 2604 OID 17630)
-- Name: recipe_audit recipe_audit_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_audit ALTER COLUMN recipe_audit_id SET DEFAULT nextval('public.recipe_audit_id_seq'::regclass);


--
-- TOC entry 3245 (class 2604 OID 17631)
-- Name: restaurant restaurant_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.restaurant ALTER COLUMN restaurant_id SET DEFAULT nextval('public.restaurant_id_seq'::regclass);


--
-- TOC entry 3414 (class 0 OID 17596)
-- Dependencies: 215
-- Data for Name: chef; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.chef VALUES (1, 'Deveaux', 'Thierry', 'thierry.deveaux@afpa.fr', 'c6adb2d288788e13b78c768c3f71d6cee793f0ce', 2);
INSERT INTO public.chef VALUES (2, 'Baucuse', 'Paul', 'paul.baucuse@villedelyon.fr', '4ec019ff7156de422108de305d14386656554e54', 1);
INSERT INTO public.chef VALUES (3, 'Ducasse', 'Alain', 'alain.ducasse@afpa.fr', 'd9fe438eccb53d3ad13dd963011693c542a9d1c9', 1);
INSERT INTO public.chef VALUES (4, 'Andaché', 'Sylvie', 'sylvie.andaché@afpa.fr', 'ca091c85e8bfa8268db92b9ee4a29cd16a019a91', 2);


--
-- TOC entry 3415 (class 0 OID 17599)
-- Dependencies: 216
-- Data for Name: comment; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.comment VALUES (2, 1, 'Très bon.', 5, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (3, 1, 'C''est dégueu.', 2, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (1, 2, 'Pas mal.', 3, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (2, 2, 'Je pense que ça mériterait plus de cuisson.', 3, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (3, 2, 'Moi j''aime bien.', 4, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (4, 3, 'Insane.', 5, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (1, 3, 'Banger.', 5, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (2, 3, 'Masterclass.', 5, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (3, 4, 'Pas terrible.', 2, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (4, 4, 'J''aurais mis plus de beurre.', 3, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (1, 4, 'Très sec.', 2, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (2, 5, 'Simple et efficace.', 4, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (3, 5, 'On va pas se mentir, c''est juste des crevettes quoi.', 3, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (4, 5, 'Pas ma préférée.', 2, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (2, 6, 'Toutes les saveurs de ma patrie.', 5, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (3, 6, 'Vé la pitchoune.', 4, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (4, 7, 'Personne n''aime ça. Supprime.', 1, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (2, 7, 'Non.', 1, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (3, 8, 'Qui peut battre une tarte aux pommes? Personne.', 5, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (4, 8, 'Manque de sucre.', 4, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (2, 8, 'Je préfère la fraise.', 3, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (3, 9, 'Manque de cuisson. Elle est trop liquide.', 3, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (4, 9, 'Trop bon, sa mère.', 5, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (1, 6, 'Ca me rappelle mon enfance. Le soleil qui brille. La douce chanson des grillons. La tramontane qui te rend fada. J''adore le piment d''espelette.', 5, '2023-04-24 10:01:48');
INSERT INTO public.comment VALUES (1, 9, 'Si j''ai pas de ramequins, je fais quoi ? T''as cru j''étais riche ?', 1, '2023-04-24 10:20:27');
INSERT INTO public.comment VALUES (1, 8, 'Pas mal.', 3, '2023-04-24 10:21:06');
INSERT INTO public.comment VALUES (1, 7, 'En vrai, si ça va.', 3, '2023-04-25 11:19:24');
INSERT INTO public.comment VALUES (1, 1, 'Je me note moi-même.', 2, '2023-04-25 15:06:04');


--
-- TOC entry 3416 (class 0 OID 17604)
-- Dependencies: 217
-- Data for Name: component; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.component VALUES (5, 'Poulet', 1, NULL, 2);
INSERT INTO public.component VALUES (6, 'Gousse d''ail', 3, NULL, 2);
INSERT INTO public.component VALUES (7, 'Citron', 1, NULL, 2);
INSERT INTO public.component VALUES (8, 'Branche de romarin', 1, NULL, 2);
INSERT INTO public.component VALUES (9, 'Beurre', 50, 'g', 2);
INSERT INTO public.component VALUES (10, 'Huile d''olive', NULL, NULL, 2);
INSERT INTO public.component VALUES (11, 'Quinoa', 200, 'g', 3);
INSERT INTO public.component VALUES (12, 'Avocat', 2, NULL, 3);
INSERT INTO public.component VALUES (13, 'Noix', 25, 'g', 3);
INSERT INTO public.component VALUES (14, 'Citron vert', 1, NULL, 3);
INSERT INTO public.component VALUES (15, 'Huile d''olive', 5, 'cL', 3);
INSERT INTO public.component VALUES (16, 'Crevette', 500, 'g', 5);
INSERT INTO public.component VALUES (17, 'Gousse d''ail', 4, NULL, 5);
INSERT INTO public.component VALUES (18, 'Bouquet de persil frais', 1, NULL, 5);
INSERT INTO public.component VALUES (19, 'Citron', 0.5, NULL, 5);
INSERT INTO public.component VALUES (20, 'Huile d''olive', 5, 'cL', 5);
INSERT INTO public.component VALUES (21, 'Aubergine', 2, NULL, 6);
INSERT INTO public.component VALUES (22, 'Courgette', 2, NULL, 6);
INSERT INTO public.component VALUES (23, 'Poivron rouge', 2, NULL, 6);
INSERT INTO public.component VALUES (24, 'Oignon', 2, NULL, 6);
INSERT INTO public.component VALUES (25, 'Gousse d''ail', 4, NULL, 6);
INSERT INTO public.component VALUES (26, 'Tomate', 4, NULL, 6);
INSERT INTO public.component VALUES (27, 'Huile d''olive', 5, 'cL', 6);
INSERT INTO public.component VALUES (28, 'Riz arborio', 300, 'g', 4);
INSERT INTO public.component VALUES (29, 'Oignon', 1, NULL, 4);
INSERT INTO public.component VALUES (30, 'Gousse d''ail', 2, NULL, 4);
INSERT INTO public.component VALUES (31, 'Champignons de Paris', 200, 'g', 4);
INSERT INTO public.component VALUES (32, 'Bouillon de légume', 1, 'L', 4);
INSERT INTO public.component VALUES (33, 'Beurre', 50, 'g', 4);
INSERT INTO public.component VALUES (34, 'Parmesan râpé', 50, 'g', 4);
INSERT INTO public.component VALUES (35, 'Epinards frais', 200, 'g', 7);
INSERT INTO public.component VALUES (36, 'Feta', 200, 'g', 7);
INSERT INTO public.component VALUES (37, 'Oignon rouge', 0.5, NULL, 7);
INSERT INTO public.component VALUES (38, 'Noix', 25, 'g', 7);
INSERT INTO public.component VALUES (39, 'Huile d''olive', 5, 'cL', 7);
INSERT INTO public.component VALUES (40, 'Vinaigre balsamique', 2.5, 'cL', 7);
INSERT INTO public.component VALUES (47, 'Crème liquide', 500, 'mL', 9);
INSERT INTO public.component VALUES (48, 'Jaune d''oeufs', 6, NULL, 9);
INSERT INTO public.component VALUES (49, 'Sucre', 100, 'g', 9);
INSERT INTO public.component VALUES (50, 'Gousse de vanille', 1, NULL, 9);
INSERT INTO public.component VALUES (51, 'Cassonade', 20, 'g', 9);
INSERT INTO public.component VALUES (64, 'Ail', 0, '', 1);
INSERT INTO public.component VALUES (41, 'Pâte feuilletée', 1, '', 8);
INSERT INTO public.component VALUES (42, 'Pomme', 6, '', 8);
INSERT INTO public.component VALUES (43, 'Sucre', 50, 'g', 8);
INSERT INTO public.component VALUES (44, 'Beurre', 50, 'g', 8);
INSERT INTO public.component VALUES (45, 'Cannelle en poudre', 2, 'g', 8);
INSERT INTO public.component VALUES (46, 'Oeufs', 5, '', 8);
INSERT INTO public.component VALUES (1, 'Asperges blanches', 1.2, 'kg', 1);
INSERT INTO public.component VALUES (2, 'Crème fraîche', 200, 'g', 1);
INSERT INTO public.component VALUES (3, 'Roquefort', 300, 'g', 1);
INSERT INTO public.component VALUES (4, 'Gruyère rapé', 100, 'g', 1);


--
-- TOC entry 3418 (class 0 OID 17608)
-- Dependencies: 219
-- Data for Name: recipe; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.recipe VALUES (2, 'Poulet rôti aux herbes de Provence', '01:30:00', 4, 'PLAT', false, 2, '{"Préchauffer le four à 180°C.","Assaisonner le poulet avec du sel, du poivre et des herbes de Provence.","Disposer le poulet dans un plat allant au four et enfourner pour 1h30 environ.","Arroser régulièrement le poulet avec son jus pendant la cuisson.","Servir chaud avec des légumes grillés ou une salade."}');
INSERT INTO public.recipe VALUES (3, 'Salade de quinoa à l’avocat et aux noix', '00:20:00', 2, 'ENTREE', true, 3, '{"Faire cuire le quinoa dans une casserole d’eau bouillante salée pendant 15 minutes.","Laisser refroidir le quinoa et l’assaisonner avec de l’huile d’olive et du vinaigre balsamique.","Couper l’avocat en cubes et les noix en petits morceaux.","Ajouter l’avocat et les noix au quinoa et mélanger délicatement.","Servir frais et garnir de feuilles de coriandre."}');
INSERT INTO public.recipe VALUES (4, 'Risotto aux champignons et au parmesan', '00:40:00', 4, 'PLAT', true, 4, '{"Faire chauffer le bouillon de légumes dans une casserole.","Dans une autre casserole, faire revenir l’oignon dans de l’huile d’olive.","Ajouter le riz et remuer jusqu’à ce qu’il soit translucide.","Ajouter le vin blanc et remuer jusqu’à évaporation.","Ajouter une louche de bouillon et remuer jusqu’à absorption.","Répéter l’opération jusqu’à ce que le riz soit al dente.","Ajouter les champignons et le parmesan râpé et mélanger.","Servir chaud avec une pincée de persil frais."}');
INSERT INTO public.recipe VALUES (7, 'Salade d’épinards et de feta', '00:15:00', 2, 'ENTREE', true, 4, '{"Laver et égoutter les épinards.","Couper des tomates cerises en deux et des concombres en rondelles.","Emietter de la feta sur les épinards.","Ajouter les tomates cerises et les concombres.","Assaisonner avec de l’huile d’olive, du vinaigre balsamique, du sel et du poivre."}');
INSERT INTO public.recipe VALUES (9, 'Crème brûlée à la vanille', '02:30:00', 4, 'DESSERT', false, 3, '{"Préchauffer le four à 150°C.","Faire chauffer de la crème liquide et de la vanille dans une casserole jusqu’à frémissement.","Mélanger des jaunes d’œufs et du sucre dans un bol.","Verser lentement la crème chaude sur le mélange d’œufs tout en remuant.","Répartir la crème dans des ramequins.","Placer les ramequins dans un plat allant au four et ajouter de l’eau chaude pour créer un bain-marie.","Enfourner pendant environ 1 heure jusqu’à ce que la crème soit prise.","Réfrigérer pendant au moins 1 heure."}');
INSERT INTO public.recipe VALUES (5, 'Crevettes grillées à l’ail et au persil', '00:20:00', 2, 'PLAT', false, 2, '{"Préchauffer le grill du four à température élevée.","Mélanger de l’ail émincé, du persil haché, du jus de citron et de l’huile d’olive dans un bol.","Badigeonner les crevettes de la marinade et les disposer sur une plaque de cuisson.","Griller les crevettes pendant environ 5 minutes de chaque côté.","Servir chaud avec du riz et une salade."}');
INSERT INTO public.recipe VALUES (6, 'Ratatouille provençale', '00:45:00', 4, 'PLAT', true, 3, '{"Chauffer de l’huile d’olive dans une grande poêle.","Ajouter de l’ail émincé, des oignons émincés et des poivrons coupés en dés.","Faire revenir jusqu’à ce que les légumes soient tendres.","Ajouter des tomates coupées en dés, des courgettes coupées en rondelles et des aubergines coupées en dés.","Saler, poivrer et ajouter des herbes de Provence.","Laisser mijoter à feu doux pendant environ 30 minutes.","Servir chaud ou froid."}');
INSERT INTO public.recipe VALUES (1, 'Grâtin d’asperges au roquefort', '00:25:00', 10, 'PLAT', true, 1, '{"Faire cuire les asperges 20 minutes dans de l’eau bouillante.","Bien égoutter les asperges et les disposer dans un plat à gratin.","Emietter le roquefort et le mélanger à la crème fraîche. Poivrer et saler.","Verser le mélange sur les asperges et répartir du gruyère pour faire gratiner.","Enfourner à 180°C (thermostat 6) pendant 15 min environ."}');
INSERT INTO public.recipe VALUES (8, 'Tarte à la pomme', '01:00:00', 8, 'DESSERT', true, 1, '{"Préchauffer le four à 180°C.","Étaler de la pâte brisée dans un moule à tarte.","Piquer le fond de la tarte avec une fourchette.","Disposer des tranches de pommes sur le fond de la tarte.","Saupoudrer de sucre et de cannelle.","Ajouter des morceaux de beurre sur le dessus.","Enfourner pendant environ 40 minutes.","Servir chaud ou froid."}');


--
-- TOC entry 3419 (class 0 OID 17614)
-- Dependencies: 220
-- Data for Name: recipe_audit; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.recipe_audit VALUES (1, 1, 'UPDATE', '2023-04-24 11:09:45.215904', 'Grâtin d’asperges au roquefort', '00:25:00', 10, 'PLAT', true, 1, '{"Faire cuire les asperges 20 minutes dans de l’eau bouillante.","Bien égoutter les asperges et les disposer dans un plat à gratin.","Emietter le roquefort et le mélanger à la crème fraîche. Poivrer et saler.","Verser le mélange sur les asperges et répartir du gruyère pour faire gratiner.","Enfourner à 180°C (thermostat 6) pendant 15 min environ."}', '{test}', '{0}', '{""}');
INSERT INTO public.recipe_audit VALUES (2, 1, 'UPDATE', '2023-04-24 11:47:23.807071', 'Grâtin d’asperges au roquefort', '00:25:00', 10, 'PLAT', true, 1, '{"Faire cuire les asperges 20 minutes dans de l’eau bouillante.","Bien égoutter les asperges et les disposer dans un plat à gratin.","Emietter le roquefort et le mélanger à la crème fraîche. Poivrer et saler.","Verser le mélange sur les asperges et répartir du gruyère pour faire gratiner.","Enfourner à 180°C (thermostat 6) pendant 15 min environ."}', '{test}', '{0}', '{""}');
INSERT INTO public.recipe_audit VALUES (3, 1, 'UPDATE', '2023-04-24 11:48:14.363666', 'Grâtin d’asperges au roquefort', '00:25:00', 10, 'PLAT', true, 1, '{"Faire cuire les asperges 20 minutes dans de l’eau bouillante.","Bien égoutter les asperges et les disposer dans un plat à gratin.","Emietter le roquefort et le mélanger à la crème fraîche. Poivrer et saler.","Verser le mélange sur les asperges et répartir du gruyère pour faire gratiner.","Enfourner à 200°C (thermostat 6) pendant 15 min environ."}', '{test}', '{0}', '{""}');
INSERT INTO public.recipe_audit VALUES (4, 8, 'UPDATE', '2023-04-24 11:50:09.059365', 'Tarte aux pommes', '01:00:00', 8, 'DESSERT', true, 1, '{"Préchauffer le four à 180°C.","Étaler de la pâte brisée dans un moule à tarte.","Piquer le fond de la tarte avec une fourchette.","Disposer des tranches de pommes sur le fond de la tarte.","Saupoudrer de sucre et de cannelle.","Ajouter des morceaux de beurre sur le dessus.","Enfourner pendant environ 40 minutes.","Servir chaud ou froid."}', '{test}', '{0}', '{""}');
INSERT INTO public.recipe_audit VALUES (7, 8, 'UPDATE', '2023-04-24 14:28:09.752858', 'Tarte à la pomme', '01:00:00', 8, 'DESSERT', true, 1, '{"Préchauffer le four à 180°C.","Étaler de la pâte brisée dans un moule à tarte.","Piquer le fond de la tarte avec une fourchette.","Disposer des tranches de pommes sur le fond de la tarte.","Saupoudrer de sucre et de cannelle.","Ajouter des morceaux de beurre sur le dessus.","Enfourner pendant environ 40 minutes.","Servir chaud ou froid."}', '{"Pâte feuilletée",Pomme,Sucre,Beurre,"Cannelle en poudre",Oeuf}', '{1,6,50,50,2,1}', '{"","",g,g,g,""}');
INSERT INTO public.recipe_audit VALUES (8, 1, 'UPDATE', '2023-04-24 14:35:14.53801', 'Grâtin d’asperges au roquefort', '00:25:00', 10, 'PLAT', true, 1, '{"Faire cuire les asperges 20 minutes dans de l’eau bouillante.","Bien égoutter les asperges et les disposer dans un plat à gratin.","Emietter le roquefort et le mélanger à la crème fraîche. Poivrer et saler.","Verser le mélange sur les asperges et répartir du gruyère pour faire gratiner.","Enfourner à 180°C (thermostat 6) pendant 15 min environ."}', '{"Asperges blanches","Crème fraîche",Roquefort,"Gruyère rapé",Ail}', '{1.2,200,300,100,0}', '{kg,g,g,g,""}');


--
-- TOC entry 3422 (class 0 OID 17622)
-- Dependencies: 223
-- Data for Name: restaurant; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.restaurant VALUES (1, 'Le Délicieux', '25 rue de la bonne fourchette', 'Lyon', '08145264874', 'ledelicieux@gmail.com');
INSERT INTO public.restaurant VALUES (2, 'Le Cinq', '5 rue du Pentagone', 'Rochefort', '08142639748', 'lecinq@gmail.com');


--
-- TOC entry 3437 (class 0 OID 0)
-- Dependencies: 218
-- Name: component_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.component_id_seq', 84, true);


--
-- TOC entry 3438 (class 0 OID 0)
-- Dependencies: 221
-- Name: recipe_audit_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recipe_audit_id_seq', 28, true);


--
-- TOC entry 3439 (class 0 OID 0)
-- Dependencies: 222
-- Name: recipe_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.recipe_id_seq', 32, true);


--
-- TOC entry 3440 (class 0 OID 0)
-- Dependencies: 224
-- Name: restaurant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.restaurant_id_seq', 5, true);


--
-- TOC entry 3441 (class 0 OID 0)
-- Dependencies: 225
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 5, false);


--
-- TOC entry 3251 (class 2606 OID 17633)
-- Name: comment comment_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_pkey PRIMARY KEY (chef_id, recipe_id);


--
-- TOC entry 3253 (class 2606 OID 17635)
-- Name: component component_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT component_pkey PRIMARY KEY (component_id);


--
-- TOC entry 3257 (class 2606 OID 17637)
-- Name: recipe_audit recipe_audit_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_audit
    ADD CONSTRAINT recipe_audit_pkey PRIMARY KEY (recipe_audit_id);


--
-- TOC entry 3255 (class 2606 OID 17639)
-- Name: recipe recipe_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT recipe_pkey PRIMARY KEY (recipe_id);


--
-- TOC entry 3259 (class 2606 OID 17641)
-- Name: restaurant restaurant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.restaurant
    ADD CONSTRAINT restaurant_pkey PRIMARY KEY (restaurant_id);


--
-- TOC entry 3249 (class 2606 OID 17643)
-- Name: chef user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chef
    ADD CONSTRAINT user_pkey PRIMARY KEY (chef_id);


--
-- TOC entry 3269 (class 2620 OID 17644)
-- Name: recipe delete_components_before_delete; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER delete_components_before_delete BEFORE DELETE ON public.recipe FOR EACH ROW EXECUTE FUNCTION public.delete_components();


--
-- TOC entry 3270 (class 2620 OID 17645)
-- Name: recipe recipe_audit_insert_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER recipe_audit_insert_trigger AFTER INSERT ON public.recipe FOR EACH ROW EXECUTE FUNCTION public.recipe_audit_insert();


--
-- TOC entry 3271 (class 2620 OID 17646)
-- Name: recipe recipe_audit_update_trigger; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER recipe_audit_update_trigger AFTER UPDATE ON public.recipe FOR EACH ROW EXECUTE FUNCTION public.recipe_audit_update();


--
-- TOC entry 3267 (class 2620 OID 17647)
-- Name: comment trigger_comment_added; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_comment_added BEFORE INSERT ON public.comment FOR EACH ROW EXECUTE FUNCTION public.comment_changed();


--
-- TOC entry 3268 (class 2620 OID 17648)
-- Name: comment trigger_comment_changed; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER trigger_comment_changed BEFORE UPDATE ON public.comment FOR EACH ROW EXECUTE FUNCTION public.comment_changed();


--
-- TOC entry 3261 (class 2606 OID 17649)
-- Name: comment comment_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES public.recipe(recipe_id);


--
-- TOC entry 3262 (class 2606 OID 17654)
-- Name: comment comment_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.comment
    ADD CONSTRAINT comment_user_id_fkey FOREIGN KEY (chef_id) REFERENCES public.chef(chef_id);


--
-- TOC entry 3263 (class 2606 OID 17659)
-- Name: component component_recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.component
    ADD CONSTRAINT component_recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES public.recipe(recipe_id);


--
-- TOC entry 3265 (class 2606 OID 17664)
-- Name: recipe_audit recipe_chef_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_audit
    ADD CONSTRAINT recipe_chef_id_fkey FOREIGN KEY (chef_id) REFERENCES public.chef(chef_id);


--
-- TOC entry 3266 (class 2606 OID 17669)
-- Name: recipe_audit recipe_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe_audit
    ADD CONSTRAINT recipe_id_fkey FOREIGN KEY (recipe_id) REFERENCES public.recipe(recipe_id);


--
-- TOC entry 3264 (class 2606 OID 17674)
-- Name: recipe recipe_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.recipe
    ADD CONSTRAINT recipe_user_id_fkey FOREIGN KEY (chef_id) REFERENCES public.chef(chef_id);


--
-- TOC entry 3260 (class 2606 OID 17679)
-- Name: chef user_restaurant_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chef
    ADD CONSTRAINT user_restaurant_id_fkey FOREIGN KEY (restaurant_id) REFERENCES public.restaurant(restaurant_id);


--
-- TOC entry 3431 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: pg_database_owner
--

REVOKE USAGE ON SCHEMA public FROM PUBLIC;


-- Completed on 2023-05-11 11:38:51

--
-- PostgreSQL database dump complete
--

